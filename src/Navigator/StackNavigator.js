import React, {useRef} from 'react';
import {
  CardStyleInterpolators,
  createStackNavigator,
} from '@react-navigation/stack';
import {TransitionPresets} from '@react-navigation/stack';

import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import Splash from '../Screens/Splash';
import OnBoarding from '../Screens/OnBoarding';
import Login from '../Screens/Login';
import Ragister from '../Screens/Ragister';
import Otp from '../Screens/Otp';
import Ragister1 from '../Screens/Ragister1';
import Ragister2 from '../Screens/Ragister2';
import Ragister3 from '../Screens/Ragister3';
import Ragister4 from '../Screens/Ragister4';
import Ragister5 from '../Screens/Ragister5';
import AccountCreated from '../Screens/AccountCreated';
import Home from '../Screens/Home';
import Explore from '../Screens/Explore';
import Chat from '../Screens/Chat';
import Profile from '../Screens/Profile';
import TabNavigator from './TabNavigator';
import Blog from '../Screens/Blog';
import OtherProfile from '../Screens/OtherProfile';
import Followers from '../Screens/Followers';
import Rating from '../Screens/Rating';
import EditProfile from '../Screens/EditProfile';
import SelfProfile from '../Screens/SelfProfile';
import MyDiamond from '../Screens/MyDiamond';
import Support from '../Screens/Support';
import Setting from '../Screens/Setting';
import About from '../Screens/About';
import Privacy from '../Screens/Privacy';
import Terms from '../Screens/Terms';
import PrivateAlbum from '../Screens/PrivateAlbum';
import NewAlbum from '../Screens/NewAlbum';
import SetDiamond from '../Screens/SetDiamond';
import Success from '../Screens/Success';
import MyEarning from '../Screens/MyEarning';
import RequestPayout from '../Screens/RequestPayout';
import PaymentSuccess from '../Screens/PaymentSuccess';
import PrivatePopup from '../Screens/PrivatePopup';
const Stack = createStackNavigator();
const StackNavigator = () => {
  const forFade = ({current}) => ({
    cardStyle: {
      opacity: current.progress,
    },
  });

  const screenOptionStyle = {
    // headerShown: false,
    ...TransitionPresets.SlideFromRightIOS,
  };
  const navigationRef = useRef();
  return (
    <NavigationContainer ref={navigationRef}>
      <Stack.Navigator
        initialRouteName={'Splash'}
        screenOptions={screenOptionStyle}>
        <Stack.Screen
          name="Splash"
          component={Splash}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="OnBoarding"
          component={OnBoarding}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Login"
          component={Login}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Ragister"
          component={Ragister}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Otp"
          component={Otp}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Ragister1"
          component={Ragister1}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Ragister2"
          component={Ragister2}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Ragister3"
          component={Ragister3}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Ragister4"
          component={Ragister4}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Ragister5"
          component={Ragister5}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="AccountCreated"
          component={AccountCreated}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Home"
          component={Home}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Explore"
          component={Explore}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Chat"
          component={Chat}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Profile"
          component={Profile}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="TabNavigator"
          component={TabNavigator}
          gestureDirection="flip"
          options={{
            headerShown: false,
            cardStyleInterpolator:
              CardStyleInterpolators.forRevealFromBottomAndroid,
          }}
        />
        <Stack.Screen
          name="Blog"
          component={Blog}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="OtherProfile"
          component={OtherProfile}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Followers"
          component={Followers}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Rating"
          component={Rating}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="EditProfile"
          component={EditProfile}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="SelfProfile"
          component={SelfProfile}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="MyDiamond"
          component={MyDiamond}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Support"
          component={Support}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Setting"
          component={Setting}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="About"
          component={About}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Privacy"
          component={Privacy}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Terms"
          component={Terms}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="PrivateAlbum"
          component={PrivateAlbum}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="NewAlbum"
          component={NewAlbum}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="SetDiamond"
          component={SetDiamond}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Success"
          component={Success}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="PaymentSuccess"
          component={PaymentSuccess}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="MyEarning"
          component={MyEarning}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="RequestPayout"
          component={RequestPayout}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="PrivatePopup"
          component={PrivatePopup}
          options={{headerShown: false}}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};
export default StackNavigator;
