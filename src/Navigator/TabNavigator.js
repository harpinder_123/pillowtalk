import React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {Image, StyleSheet} from 'react-native';
import Home from '../Screens/Home';
import Explore from '../Screens/Explore';
import Chat from '../Screens/Chat';
import Profile from '../Screens/Profile';
import Blog from '../Screens/Blog';

const iconPath = {
  h: require('../images/home.png'),
  ha: require('../images/home1.png'),
  s: require('../images/4-dot1.png'),
  sa: require('../images/4-dot.png'),
  f: require('../images/plus-circle.png'),
  fa: require('../images/plus-circle1.png'),
  p: require('../images/chat1.png'),
  pa: require('../images/chat.png'),
  a: require('../images/bottom-user1.png'),
  Aa: require('../images/bottom-user.png'),
};

const Tab = createBottomTabNavigator();
const TabIcon = source => <Image source={source} style={styles.tabIcon} />;
const TabPlus = source => <Image source={source} style={styles.tabPlus} />;

const TabNavigator = () => {
  return (
    <Tab.Navigator
      initialRouteName="Home"
      tabBarOptions={{
        keyboardHidesTabBar: true,
        labelStyle: {
          paddingBottom: 2,
          fontSize: 10,
          fontFamily: 'Avenir-Roman',
          fontWeight: '400',
        },
        activeTintColor: '#6CBDFF',
        activeBackgroundColor: '#FFFFFF',
        inactiveBackgroundColor: '#FFFFFF',
      }}>
      <Tab.Screen
        name="Home"
        component={Home}
        options={{
          tabBarLabel: '',
          tabBarIcon: ({focused}) =>
            TabIcon(focused ? iconPath.h : iconPath.ha),
          headerShown: false,
        }}
      />

      <Tab.Screen
        name="Explore"
        component={Explore}
        options={{
          tabBarLabel: '',
          tabBarIcon: ({focused}) =>
            TabIcon(focused ? iconPath.s : iconPath.sa),
          headerShown: false,
        }}
      />

      <Tab.Screen
        name="Blog"
        component={Blog}
        options={{
          tabBarLabel: '',
          tabBarIcon: ({focused}) =>
            TabPlus(focused ? iconPath.f : iconPath.fa),
          headerShown: false,
        }}
      />

      <Tab.Screen
        name="Chat"
        component={Chat}
        options={{
          tabBarLabel: '',
          tabBarIcon: ({focused}) =>
            TabIcon(focused ? iconPath.p : iconPath.pa),
          headerShown: false,
        }}
      />

      <Tab.Screen
        name="Profile"
        component={Profile}
        options={{
          tabBarLabel: '',
          tabBarIcon: ({focused}) =>
            TabIcon(focused ? iconPath.a : iconPath.Aa),
          headerShown: false,
        }}
      />
    </Tab.Navigator>
  );
};
export default TabNavigator;

const styles = StyleSheet.create({
  tabIcon: {
    height: 20,
    width: 20,
    resizeMode: 'contain',
  },
  tabPlus: {
    height: 60,
    width: 60,
    marginTop: -30,
    resizeMode: 'contain',
  },
});
