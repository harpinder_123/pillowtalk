import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
  ImageBackground,
  ProgressBarAndroidBase,
  TextInput,
  FlatList,
} from 'react-native';
import {StatusBarDark} from '../Custom/CustomStatusBar';
import {Header, MainView} from '../Custom/CustomView';
import * as Progress from 'react-native-progress';
import LinearGradient from 'react-native-linear-gradient';
const {height, width} = Dimensions.get('window');

const Ragister3 = ({navigation}) => {
  const [select, setSelect] = useState(0);
  const [data, setData] = useState([
    {title: 'Male'},
    {title: 'Female'},
    {title: 'Both'},
  ]);

  return (
    <View style={{backgroundColor: '#fff', flex: 1}}>
      <StatusBarDark />
      <Header onPress={() => navigation.goBack()} />
      <ScrollView>
        <ImageBackground
          style={styles.image}
          source={require('../images/background.png')}>
          <Progress.Bar
            progress={0.6}
            width={330}
            color={'#3023AE'}
            style={{
              borderColor: '#D8D8D8',
              backgroundColor: '#D8D8D8',
              alignSelf: 'center',
              marginTop: 20,
            }}
          />
          <Text style={styles.basicText}>Interest in</Text>
          <View style={{marginTop: 60}}>
            <FlatList
              numColumns={1}
              data={data}
              renderItem={({item, index}) => (
                <TouchableOpacity
                  onPress={() => setSelect(index)}
                  style={{
                    width: width / 1.1 - 25,
                    paddingHorizontal: 10,
                    paddingVertical: 8,
                    borderRadius: 16,
                    marginTop: 20,
                    margin: 5,
                    marginLeft: 30,
                    backgroundColor: index == select ? '#3023AE' : '#ACB1C0',
                  }}>
                  <Text
                    style={{
                      fontFamily: 'Nunito',
                      fontSize: 14,
                      fontWeight: '600',
                      color: index == select ? 'white' : 'black',
                      // marginTop: 10,
                      textAlign: 'center',
                    }}>
                    {item.title}
                  </Text>
                </TouchableOpacity>
              )}
            />
          </View>
          <Text style={styles.loremText}>
            **Lorem Ipsum is simply dummy text of the printing{'\n'} and
            typesetting industry.
          </Text>

          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              marginHorizontal: 20,
            }}>
            <Text style={styles.skip}>SKIP</Text>
            <TouchableOpacity
              activeOpacity={0.8}
              onPress={() => navigation.navigate('Ragister4')}
              style={styles.touch}>
              <Text style={styles.touchText}>NEXT</Text>
            </TouchableOpacity>
          </View>
        </ImageBackground>
      </ScrollView>
    </View>
  );
};

export default Ragister3;

const styles = StyleSheet.create({
  image: {
    width: '100%',
    height: '100%',
    resizeMode: 'contain',
    alignSelf: 'center',
  },
  basicText: {
    fontFamily: 'Nunito',
    fontSize: 34,
    fontWeight: '900',
    color: '#000000',
    marginTop: 60,
    textAlign: 'center',
  },
  containerImg: {
    width: 90,
    height: 90,
    alignSelf: 'center',
  },
  loremText: {
    fontFamily: 'Nunito',
    fontSize: 14,
    fontWeight: '900',
    color: '#FF0000',
    marginTop: 20,
    marginLeft: 30,
  },
  skip: {
    fontFamily: 'Nunito',
    fontSize: 20,
    fontWeight: '700',
    color: '#000000',
    marginTop: '70%',
  },
  touch: {
    width: '35%',
    height: 58,
    backgroundColor: '#3023AE',
    borderRadius: 30,
    marginTop: '65%',
    marginBottom: 20,
  },
  touchText: {
    fontFamily: 'Nunito',
    fontSize: 22,
    fontWeight: '700',
    color: '#fff',
    textAlign: 'center',
    marginTop: 12,
  },
});
