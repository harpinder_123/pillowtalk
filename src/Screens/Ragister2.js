import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
  ImageBackground,
  ProgressBarAndroidBase,
  TextInput,
  FlatList,
} from 'react-native';
import {StatusBarDark} from '../Custom/CustomStatusBar';
import {Header, MainView} from '../Custom/CustomView';
import * as Progress from 'react-native-progress';
import LinearGradient from 'react-native-linear-gradient';
const {height, width} = Dimensions.get('window');

const Ragister2 = ({navigation}) => {
  const [select, setSelect] = useState(0);
  const [data, setData] = useState([
    {id: '1', title: 'Music'},
    {id: '2', title: 'Photo'},
    {id: '3', title: 'Photography'},
  ]);

  const [select1, setSelect1] = useState(0);
  const [num, setNum] = useState([
    {id: '1', title: 'Design'},
    {id: '2', title: 'Art Film'},
    {id: '3', title: 'Dancing'},
  ]);

  const [select2, setSelect2] = useState(0);
  const [col, setCol] = useState([
    {id: '1', title: 'Fashion Design'},
    {id: '2', title: 'Listen Music'},
  ]);
  return (
    <View style={{backgroundColor: '#fff', flex: 1}}>
      <StatusBarDark />
      <Header onPress={() => navigation.goBack()} />
      <ScrollView>
        <ImageBackground
          style={styles.image}
          source={require('../images/background.png')}>
          <Progress.Bar
            progress={0.5}
            width={330}
            color={'#3023AE'}
            style={{
              borderColor: '#D8D8D8',
              backgroundColor: '#D8D8D8',
              alignSelf: 'center',
              marginTop: 20,
            }}
          />
          <Text style={styles.basicText}>
            Choose your
            {'\n'}interest
          </Text>
          <View>
            <FlatList
              numColumns={3}
              keyExtractor={item => item.id}
              data={data}
              renderItem={({item, index}) => (
                <TouchableOpacity
                  onPress={() => setSelect(index)}
                  style={{
                    width: width / 3 - 25,
                    paddingHorizontal: 10,
                    paddingVertical: 8,
                    borderRadius: 16,
                    marginTop: 30,
                    margin: 5,
                    marginLeft: 15,
                    backgroundColor: index == select ? '#3023AE' : '#ACB1C0',
                  }}>
                  <Text
                    style={{
                      fontFamily: 'Nunito',
                      fontSize: 14,
                      fontWeight: '600',
                      color: index == select ? 'white' : 'black',
                      // marginTop: 10,
                      textAlign: 'center',
                    }}>
                    {item.title}
                  </Text>
                </TouchableOpacity>
              )}
            />
          </View>

          <View>
            <FlatList
              numColumns={3}
              keyExtractor={item => item.id}
              data={num}
              renderItem={({item, index}) => (
                <TouchableOpacity
                  onPress={() => setSelect1(index)}
                  style={{
                    width: width / 3 - 25,
                    paddingHorizontal: 10,
                    paddingVertical: 8,
                    borderRadius: 16,
                    marginTop: 30,
                    margin: 5,
                    marginLeft: 15,
                    backgroundColor: index == select1 ? '#3023AE' : '#ACB1C0',
                  }}>
                  <Text
                    style={{
                      fontFamily: 'Nunito',
                      fontSize: 14,
                      fontWeight: '600',
                      color: index == select1 ? 'white' : 'black',
                      // marginTop: 10,
                      textAlign: 'center',
                    }}>
                    {item.title}
                  </Text>
                </TouchableOpacity>
              )}
            />
          </View>

          <View>
            <FlatList
              numColumns={2}
              keyExtractor={item => item.id}
              data={col}
              renderItem={({item, index}) => (
                <TouchableOpacity
                  onPress={() => setSelect2(index)}
                  style={{
                    width: width / 2 - 25,
                    paddingHorizontal: 10,
                    paddingVertical: 8,
                    borderRadius: 16,
                    marginTop: 30,
                    margin: 5,
                    marginLeft: 15,
                    backgroundColor: index == select2 ? '#3023AE' : '#ACB1C0',
                  }}>
                  <Text
                    style={{
                      fontFamily: 'Nunito',
                      fontSize: 14,
                      fontWeight: '600',
                      color: index == select2 ? 'white' : 'black',
                      // marginTop: 10,
                      textAlign: 'center',
                    }}>
                    {item.title}
                  </Text>
                </TouchableOpacity>
              )}
            />
          </View>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              marginHorizontal: 20,
            }}>
            <Text style={styles.skip}>SKIP</Text>
            <TouchableOpacity
              activeOpacity={0.8}
              onPress={() => navigation.navigate('Ragister3')}
              style={styles.touch}>
              <Text style={styles.touchText}>NEXT</Text>
            </TouchableOpacity>
          </View>
        </ImageBackground>
      </ScrollView>
    </View>
  );
};

export default Ragister2;

const styles = StyleSheet.create({
  image: {
    width: '100%',
    height: '100%',
    resizeMode: 'contain',
    alignSelf: 'center',
  },
  basicText: {
    fontFamily: 'Nunito',
    fontSize: 34,
    fontWeight: '900',
    color: '#000000',
    marginTop: 60,
    textAlign: 'center',
  },
  containerImg: {
    width: 90,
    height: 90,
    alignSelf: 'center',
  },
  skip: {
    fontFamily: 'Nunito',
    fontSize: 20,
    fontWeight: '700',
    color: '#000000',
    marginTop: '80%',
  },
  touch: {
    width: '35%',
    height: 58,
    backgroundColor: '#3023AE',
    borderRadius: 30,
    marginTop: '75%',
    marginBottom: 20,
  },
  touchText: {
    fontFamily: 'Nunito',
    fontSize: 22,
    fontWeight: '700',
    color: '#fff',
    textAlign: 'center',
    marginTop: 12,
  },
});
