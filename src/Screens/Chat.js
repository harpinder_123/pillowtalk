import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
  useWindowDimensions,
  ImageBackgroundBase,
  ImageBackground,
  FlatList,
} from 'react-native';
import {StatusBarDark} from '../Custom/CustomStatusBar';
import {Header, MainView} from '../Custom/CustomView';
const {height} = Dimensions.get('window');

const Chat = ({navigation}) => {
  const [data, setData] = useState([
    {
      title: 'Stells Stefword',
      subtitle: 'You sent a sticker',
      time: '5:30 PM',
      source: require('../images/female.png'),
    },
    {
      title: 'Samuel Joyce',
      subtitle: 'Samuel sent a sticker',
      time: '4:00 PM',
      source: require('../images/male.png'),
    },
    {
      title: 'Jenni Miranda',
      subtitle: 'I like this version 😍🥰',
      time: '10:00 AM',
      source: require('../images/female.png'),
    },
    {
      title: 'Elva Barker',
      subtitle: 'Elva sent an attachment',
      time: '22 Aug',
      source: require('../images/female.png'),
    },
    {
      title: 'Amanda Brown',
      subtitle: 'Coo!! Go for it 😍',
      time: '21 Aug',
      source: require('../images/female.png'),
    },
    {
      title: 'Elva Barker',
      subtitle: 'Elva sent an attachment',
      time: '22 Aug',
      source: require('../images/female.png'),
    },
  ]);
  return (
    <View style={{backgroundColor: '#FFFFFF', flex: 1}}>
      <StatusBarDark />
      <Header onPress={() => navigation.goBack()} title={'Chat'} />

      <FlatList
        numColumns={1}
        data={data}
        renderItem={({item, index}) => (
          <View>
            <View style={{flexDirection: 'row'}}>
              <Image style={styles.image} source={item.source} />

              <Text style={styles.text}>{item.title}</Text>
              <Text style={styles.time}>{item.time}</Text>
            </View>
            <Text style={styles.subText}>{item.subtitle}</Text>
          </View>
        )}
      />
    </View>
  );
};

export default Chat;

const styles = StyleSheet.create({
  image: {
    width: 66,
    height: 66,
    resizeMode: 'contain',
    marginTop: 40,
    marginLeft: 20,
  },
  text: {
    fontSize: 17,
    fontFamily: 'Nunito',
    fontWeight: 'bold',
    color: '#121213',
    marginLeft: 10,
    marginTop: 45,
  },
  subText: {
    fontSize: 14,
    fontFamily: 'Nunito',
    fontWeight: '600',
    color: '#7B87A5',
    marginLeft: '25%',
    marginTop: -30,
  },
  time: {
    fontSize: 12,
    fontFamily: 'Nunito',
    fontWeight: '600',
    color: '#7B87A5',
    marginLeft: 'auto',
    marginTop: 50,
    marginRight: 20,
  },
});
