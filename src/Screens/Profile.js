import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
  ImageBackground,
} from 'react-native';
import {StatusBarDark} from '../Custom/CustomStatusBar';
import {HeaderLight, MainView} from '../Custom/CustomView';
const {height} = Dimensions.get('window');

const Profile = ({navigation}) => {
  return (
    <View style={{backgroundColor: '#F7F7F7', flex: 1}}>
      <StatusBarDark />
      <ScrollView contentContainerStyle={{flexGrow: 1}}>
        <HeaderLight />
        <TouchableOpacity onPress={() => navigation.navigate('EditProfile')}>
          <Image
            style={styles.edit}
            source={require('../images/user-edit.png')}
          />
        </TouchableOpacity>
        <View style={styles.contain}>
          <Image
            style={styles.centerImg}
            source={require('../images/male.png')}
          />
          <Text style={styles.centerText}>Steve Barnett</Text>
          <Text style={styles.centerSubText}>ID: 64983274</Text>
          <View style={styles.Line} />
          <View style={{flexDirection: 'row', justifyContent: 'space-evenly'}}>
            <View>
              <Text style={styles.bottomtext}>Followers</Text>
              <Text style={styles.bottomsubtext}>218</Text>
            </View>
            <View>
              <Text style={styles.bottomtext}>Following</Text>
              <Text style={styles.bottomsubtext}>3k1</Text>
            </View>
            <View>
              <Text style={styles.bottomtext}>Rating</Text>
              <Text style={styles.bottomsubtext}>4.0</Text>
            </View>
          </View>
        </View>
        <View style={styles.container}>
          <TouchableOpacity
            onPress={() => navigation.navigate('SelfProfile')}
            activeOpacity={0.7}
            style={styles.rowViewOffCss}>
            <View style={{flexDirection: 'row'}}>
              <Image
                style={{width: 20, height: 20, resizeMode: 'contain'}}
                source={require('../images/my-profile.png')}
              />
              <Text style={[styles.userNumberTextOffCss, {marginLeft: 15}]}>
                My Profile
              </Text>
            </View>
            <Image
              style={{width: 10, height: 15, resizeMode: 'contain'}}
              source={require('../images/arrow.png')}
            />
          </TouchableOpacity>
          <View style={styles.underLine} />

          <TouchableOpacity
            activeOpacity={0.7}
            style={styles.rowViewOffCss}
            onPress={() => navigation.navigate('MyDiamond')}>
            <View style={{flexDirection: 'row'}}>
              <Image
                style={{width: 20, height: 20, resizeMode: 'contain'}}
                source={require('../images/my-diamond.png')}
              />
              <Text style={[styles.userNumberTextOffCss, {marginLeft: 15}]}>
                My Diamond
              </Text>
            </View>
            <Image
              style={{width: 10, height: 15, resizeMode: 'contain'}}
              source={require('../images/arrow.png')}
            />
          </TouchableOpacity>
          <View style={styles.underLine} />

          <TouchableOpacity
            activeOpacity={0.7}
            style={styles.rowViewOffCss}
            onPress={() => navigation.navigate('SetDiamond')}>
            <View style={{flexDirection: 'row'}}>
              <Image
                style={{width: 20, height: 20, resizeMode: 'contain'}}
                source={require('../images/set-diamond.png')}
              />
              <Text style={[styles.userNumberTextOffCss, {marginLeft: 15}]}>
                Set Diamond
              </Text>
            </View>
            <Image
              style={{width: 10, height: 15, resizeMode: 'contain'}}
              source={require('../images/arrow.png')}
            />
          </TouchableOpacity>
          <View style={styles.underLine} />

          <TouchableOpacity
            activeOpacity={0.7}
            style={styles.rowViewOffCss}
            onPress={() => navigation.navigate('MyEarning')}>
            <View style={{flexDirection: 'row'}}>
              <Image
                style={{width: 20, height: 20, resizeMode: 'contain'}}
                source={require('../images/my-earning.png')}
              />
              <Text style={[styles.userNumberTextOffCss, {marginLeft: 15}]}>
                My Earning
              </Text>
            </View>
            <Image
              style={{width: 10, height: 15, resizeMode: 'contain'}}
              source={require('../images/arrow.png')}
            />
          </TouchableOpacity>
        </View>

        <View style={styles.container}>
          <TouchableOpacity activeOpacity={0.7} style={styles.rowViewOffCss}>
            <View style={{flexDirection: 'row'}}>
              <Image
                style={{width: 20, height: 20, resizeMode: 'contain'}}
                source={require('../images/blacklist.png')}
              />
              <Text style={[styles.userNumberTextOffCss, {marginLeft: 15}]}>
                Blacklist
              </Text>
            </View>
            <Image
              style={{width: 10, height: 15, resizeMode: 'contain'}}
              source={require('../images/arrow.png')}
            />
          </TouchableOpacity>
          <View style={styles.underLine} />

          <TouchableOpacity
            activeOpacity={0.7}
            style={styles.rowViewOffCss}
            onPress={() => navigation.navigate('Support')}>
            <View style={{flexDirection: 'row'}}>
              <Image
                style={{width: 20, height: 20, resizeMode: 'contain'}}
                source={require('../images/support.png')}
              />
              <Text style={[styles.userNumberTextOffCss, {marginLeft: 15}]}>
                Support
              </Text>
            </View>
            <Image
              style={{width: 10, height: 15, resizeMode: 'contain'}}
              source={require('../images/arrow.png')}
            />
          </TouchableOpacity>
          <View style={styles.underLine} />

          <TouchableOpacity activeOpacity={0.7} style={styles.rowViewOffCss}>
            <View style={{flexDirection: 'row'}}>
              <Image
                style={{width: 20, height: 20, resizeMode: 'contain'}}
                source={require('../images/delete-account.png')}
              />
              <Text style={[styles.userNumberTextOffCss, {marginLeft: 15}]}>
                Delete Account
              </Text>
            </View>
            <Image
              style={{width: 10, height: 15, resizeMode: 'contain'}}
              source={require('../images/arrow.png')}
            />
          </TouchableOpacity>
          <View style={styles.underLine} />

          <TouchableOpacity
            activeOpacity={0.7}
            style={styles.rowViewOffCss}
            onPress={() => navigation.navigate('Setting')}>
            <View style={{flexDirection: 'row'}}>
              <Image
                style={{width: 20, height: 20, resizeMode: 'contain'}}
                source={require('../images/setting.png')}
              />
              <Text style={[styles.userNumberTextOffCss, {marginLeft: 15}]}>
                Setting
              </Text>
            </View>
            <Image
              style={{width: 10, height: 15, resizeMode: 'contain'}}
              source={require('../images/arrow.png')}
            />
          </TouchableOpacity>
        </View>
      </ScrollView>
    </View>
  );
};

export default Profile;

const styles = StyleSheet.create({
  image: {
    width: 395,
    height: 280,
    alignSelf: 'center',
    resizeMode: 'contain',
  },
  edit: {
    width: 24,
    height: 20,
    marginTop: 20,
    marginLeft: 'auto',
    marginHorizontal: 30,
  },
  contain: {
    backgroundColor: '#fff',
    borderRadius: 8,
    padding: 10,
    marginHorizontal: 15,
    marginTop: 40,
    elevation: 5,
  },
  container: {
    backgroundColor: '#fff',
    borderRadius: 8,
    padding: 10,
    marginHorizontal: 15,
    marginTop: 20,
    elevation: 5,
    marginBottom: 5,
  },
  centerImg: {
    width: 94,
    height: 94,
    alignSelf: 'center',
    marginTop: -60,
  },
  centerText: {
    fontFamily: 'Nunito',
    fontSize: 24,
    fontWeight: '600',
    color: '#262628',
    textAlign: 'center',
    marginTop: 5,
  },
  centerSubText: {
    fontFamily: 'Nunito',
    fontSize: 15,
    fontWeight: '600',
    color: '#ACB1C0',
    textAlign: 'center',
  },
  Line: {
    height: 1,
    borderRadius: 5,
    backgroundColor: '#F5F5F5',
    marginTop: 20,
    // marginHorizontal: 30,
    // marginLeft: 25,
  },
  underLine: {
    height: 1,
    borderRadius: 5,
    backgroundColor: '#F5F5F5',
  },
  bottomtext: {
    fontFamily: 'Nunito',
    fontSize: 12,
    fontWeight: '500',
    color: '#ACB1C0',
    marginTop: 10,
    marginHorizontal: 10,
    textAlign: 'center',
  },
  bottomsubtext: {
    fontFamily: 'Nunito',
    fontSize: 20,
    fontWeight: 'bold',
    color: '#000000',
    marginTop: 5,
    marginHorizontal: 10,
    textAlign: 'center',
  },
  rowViewOffCss: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: 15,
  },
  userNumberTextOffCss: {
    fontFamily: 'Nunito',
    fontSize: 15,
    fontWeight: '600',
    color: '#262626',
  },
});
