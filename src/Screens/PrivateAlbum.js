import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
  ImageBackground,
  FlatList,
  TextInput,
} from 'react-native';
import {StatusBarDark} from '../Custom/CustomStatusBar';
import {Header, MainView} from '../Custom/CustomView';
import LinearGradient from 'react-native-linear-gradient';
const {height} = Dimensions.get('window');

const PrivateAlbum = ({navigation}) => {
  return (
    <View style={{backgroundColor: '#FFFFFF', flex: 1}}>
      <StatusBarDark />
      <Header onPress={() => navigation.goBack()} title={'Private Album'} />
      <ScrollView>
        <TextLabel title={'Album Name'} />
        <View style={styles.textInput}>
          <TextInput
            // value={state.name}
            //   onChangeText={name => setState({...state, name})}
            placeholder={'New Album'}
          />
        </View>
        <TextLabel title={'Album Price'} />
        <View style={styles.textInput}>
          <TextInput
            // value={state.name}
            //   onChangeText={name => setState({...state, name})}
            placeholder={'500/min'}
          />
        </View>
        <TextLabel title={'Description'} />
        <View style={styles.textInput2}>
          <TextInput
            // value={state.name}
            //   onChangeText={name => setState({...state, name})}
            placeholder={'Write description'}
          />
        </View>

        <TextLabel title={'Upload Photos / Videos'} />
        <View style={{flexDirection: 'row'}}>
          <ImageBackground
            style={{width: 130, height: 85, marginLeft: 30, marginTop: 20}}
            source={require('../images/ground.png')}>
            <Image
              style={{
                width: 24,
                height: 30,
                alignSelf: 'center',
                marginTop: 20,
              }}
              source={require('../images/upload1.png')}
            />
            <Text style={styles.upload}>Upload</Text>
          </ImageBackground>

          <ImageBackground
            style={{width: 130, height: 85, marginLeft: 30, marginTop: 20}}
            source={require('../images/ground.png')}>
            <Image
              style={{
                width: 25,
                height: 25,
                alignSelf: 'center',
                marginTop: 25,
              }}
              source={require('../images/plus1.png')}
            />
          </ImageBackground>
        </View>
        <TouchableOpacity
          activeOpacity={0.8}
          onPress={() => navigation.navigate('TabNavigator')}>
          <LinearGradient
            colors={['#C86DD7', '#3023AE']}
            start={{x: 0, y: 0}}
            end={{x: 1, y: 1}}
            style={styles.touchButton}>
            <Text style={styles.touchText}>SUBMIT</Text>
          </LinearGradient>
        </TouchableOpacity>
      </ScrollView>
    </View>
  );
};

export default PrivateAlbum;
const TextLabel = ({title}) => <Text style={styles.textLabel}>{title}</Text>;
const styles = StyleSheet.create({
  image: {
    width: 66,
    height: 66,
    resizeMode: 'contain',
    marginTop: 40,
    marginLeft: 20,
  },
  text: {
    fontSize: 17,
    fontFamily: 'Nunito',
    fontWeight: 'bold',
    color: '#121213',
    marginLeft: 10,
    marginTop: 45,
  },
  subText: {
    fontSize: 14,
    fontFamily: 'Nunito',
    fontWeight: '600',
    color: '#7B87A5',
    marginLeft: '25%',
    marginTop: -30,
  },
  time: {
    fontSize: 12,
    fontFamily: 'Nunito',
    fontWeight: '600',
    color: '#7B87A5',
    marginLeft: 'auto',
    marginTop: 43,
    marginRight: 20,
  },
  textInput: {
    borderRadius: 18,
    borderWidth: 1,
    paddingHorizontal: 10,
    marginHorizontal: 30,
    marginTop: 10,
    marginBottom: 0,
    fontSize: 12,
    fontFamily: 'Nunito',
    fontFamily: '500',
    backgroundColor: '#F0F0F0',
    borderColor: '#F0F0F0',
  },
  textInput2: {
    borderRadius: 18,
    borderWidth: 1,
    paddingHorizontal: 10,
    marginHorizontal: 30,
    marginTop: 10,
    marginBottom: 0,
    fontSize: 12,
    fontFamily: 'Nunito',
    fontFamily: '500',
    backgroundColor: '#F0F0F0',
    borderColor: '#F0F0F0',
    height: 100,
  },
  textLabel: {
    fontFamily: 'Nunito',
    fontWeight: '900',
    fontSize: 14,
    color: '#0E1236',
    marginHorizontal: 30,
    marginTop: 30,
    marginBottom: 0,
  },
  upload: {
    fontFamily: 'Nunito',
    fontWeight: '600',
    fontSize: 14,
    color: '#3023AE',
    textAlign: 'center',
    marginTop: 5,
  },
  touchButton: {
    padding: 10,
    marginHorizontal: 30,
    borderRadius: 23,
    marginTop: '30%',
    marginBottom: 20,
  },
  touchText: {
    fontFamily: 'Nunito',
    fontWeight: '900',
    fontSize: 16,
    color: '#fff',
    textAlign: 'center',
  },
});
