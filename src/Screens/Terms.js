import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
  useWindowDimensions,
  ImageBackgroundBase,
  ImageBackground,
  FlatList,
} from 'react-native';
import {StatusBarDark} from '../Custom/CustomStatusBar';
import {Header, MainView} from '../Custom/CustomView';
const {height} = Dimensions.get('window');

const Terms = ({navigation}) => {
  return (
    <View style={{backgroundColor: '#FFFFFF', flex: 1}}>
      <StatusBarDark />
      <Header
        onPress={() => navigation.goBack()}
        title={'Terms & Conditions'}
      />
    </View>
  );
};

export default Terms;

const styles = StyleSheet.create({
  image: {
    width: 66,
    height: 66,
    resizeMode: 'contain',
    marginTop: 40,
    marginLeft: 20,
  },
  text: {
    fontSize: 17,
    fontFamily: 'Nunito',
    fontWeight: 'bold',
    color: '#121213',
    marginLeft: 10,
    marginTop: 45,
  },
  subText: {
    fontSize: 14,
    fontFamily: 'Nunito',
    fontWeight: '600',
    color: '#7B87A5',
    marginLeft: '25%',
    marginTop: -30,
  },
  time: {
    fontSize: 12,
    fontFamily: 'Nunito',
    fontWeight: '600',
    color: '#7B87A5',
    marginLeft: 'auto',
    marginTop: 43,
    marginRight: 20,
  },
});
