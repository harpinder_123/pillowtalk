import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
  ImageBackground,
} from 'react-native';
import {StatusBarDark} from '../Custom/CustomStatusBar';
import Swiper from 'react-native-swiper';
const {height} = Dimensions.get('window');

const data = [
  {
    source: require('../images/images.png'),
    title: 'Dating better than \never before',
    titleHeader: 'We know, finding love can be hard.\nWe think it shouldn’t be',
  },
  {
    source: require('../images/images.png'),
    title: 'Dating better than \never before',
    titleHeader: 'We know, finding love can be hard.\nWe think it shouldn’t be',
  },
  {
    source: require('../images/images.png'),
    title: 'Dating better than \never before',
    titleHeader: 'We know, finding love can be hard.\nWe think it shouldn’t be',
  },
];

const OnBoarding = ({navigation}) => {
  return (
    <View style={{backgroundColor: '#FFFFFF', flex: 1}}>
      <StatusBarDark bg={'#FFFFFF'} />

      <Swiper
        onMomentumScrollEnd={(e, state, context) => {
          // console.log('index:', state.index)
        }}
        // ref="swiper"
        showsButtons={false}
        loop={false}
        dot={<View style={styles.dot} />}
        activeDot={<View style={styles.activeDot} />}
        paginationStyle={{
          resizeMode: 'contain',
          marginBottom: 30,
          marginLeft: '70%',
        }}
        showsPagination={true}>
        {data.map(item => {
          const {source, title, titleHeader} = item;
          return (
            <View>
              <ImageBackground style={styles.sub2image} source={source}>
                <Text style={styles.titletxt}>{title}</Text>
                <Text style={styles.titleHeadertext}>{titleHeader}</Text>
                <TouchableOpacity
                  activeOpacity={0.8}
                  onPress={() => navigation.navigate('Login')}
                  style={styles.touch}>
                  <Text style={styles.touchText}>Get Started</Text>
                </TouchableOpacity>
              </ImageBackground>
            </View>
          );
        })}
      </Swiper>
    </View>
  );
};

export default OnBoarding;

const styles = StyleSheet.create({
  sub2image: {
    resizeMode: 'contain',
    height: '100%',
    width: '100%',
  },
  dot: {
    backgroundColor: '#D8D8D8',
    width: 8,
    height: 8,
    borderRadius: 4,
    marginLeft: 4,
    marginRight: 4,
    marginTop: 3,
    marginBottom: 3,
  },
  activeDot: {
    backgroundColor: '#fff',
    width: 15,
    height: 8,
    borderRadius: 4,
    marginLeft: 4,
    marginRight: 4,
    marginTop: 3,
    marginBottom: 3,
  },
  titletxt: {
    fontFamily: 'Nunito',
    fontSize: 30,
    fontWeight: '900',
    color: '#fff',
    marginTop: height / 1.4,
    marginLeft: 30,
  },
  titleHeadertext: {
    fontFamily: 'Nunito',
    fontSize: 16,
    fontWeight: '500',
    color: '#fff',
    marginTop: 10,
    lineHeight: 22,
    marginLeft: 30,
  },
  touch: {
    width: '40%',
    height: 58,
    backgroundColor: '#fff',
    borderRadius: 30,
    marginLeft: 30,
    marginTop: 30,
  },
  touchText: {
    fontFamily: 'Nunito',
    fontSize: 22,
    fontWeight: '900',
    color: '#000000',
    textAlign: 'center',
    marginTop: 12,
  },
});
