import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
  FlatList,
} from 'react-native';
import {StatusBarDark} from '../Custom/CustomStatusBar';
const {height} = Dimensions.get('window');

const Home = ({navigation}) => {
  const [data, setData] = useState([
    {
      title: 'Ruby Diaz',
      subTitle: '2 hours ago',
      source: require('../images/female.png'),
    },
    {
      title: 'Martha Rojas',
      subTitle: '2 hours ago',
      source: require('../images/female.png'),
    },
  ]);
  return (
    <View style={{backgroundColor: '#F7F7F7', flex: 1}}>
      <StatusBarDark />
      <ScrollView>
        <View
          style={{
            backgroundColor: '#fff',
            height: 110,
            elevation: 5,
            flexDirection: 'row',
            justifyContent: 'space-between',
          }}>
          <Image style={styles.image} source={require('../images/logo1.png')} />
          <Image
            style={styles.notification}
            source={require('../images/notification.png')}
          />
        </View>
        <FlatList
          numColumns={1}
          data={data}
          renderItem={({item, index}) => (
            <View
              style={{
                backgroundColor: '#fff',
                paddingHorizontal: 10,
                marginTop: 20,
                elevation: 5,
              }}>
              <View style={{flexDirection: 'row'}}>
                <Image style={styles.girl} source={item.source} />
                <View>
                  <Text style={styles.girlText}>{item.title}</Text>
                  <Text style={styles.hour}>{item.subTitle}</Text>
                </View>
              </View>
              <Text style={styles.loremText}>
                Lorem Ipsum is simply dummy text of the printing….
                <Text style={{color: '#3023AE'}}>Read More</Text>
              </Text>
              <Image
                style={{
                  width: '100%',
                  height: 150,
                  resizeMode: 'stretch',
                  marginTop: 10,
                }}
                source={require('../images/Rectangle1.png')}
              />
              <View style={{flexDirection: 'row'}}>
                <Image
                  style={styles.heart}
                  source={require('../images/Like.png')}
                />
                <Text style={styles.heartText}>1125</Text>
              </View>
            </View>
          )}
        />
      </ScrollView>
    </View>
  );
};

export default Home;

const styles = StyleSheet.create({
  image: {
    width: '40%',
    height: '40%',
    resizeMode: 'contain',
    marginTop: 50,
    marginLeft: 30,
  },
  notification: {
    width: 30,
    height: 30,
    marginTop: 55,
    marginRight: 20,
  },
  girl: {
    height: 50,
    width: 50,
    marginLeft: 20,
    marginTop: 10,
  },
  girlText: {
    fontFamily: 'Nunito',
    fontWeight: 'bold',
    fontSize: 17,
    color: '#161F3D',
    marginTop: 15,
    marginLeft: 10,
  },
  hour: {
    fontFamily: 'Nunito',
    fontWeight: 'bold',
    fontSize: 13,
    color: '#0000004d',
    marginLeft: 10,
  },
  loremText: {
    fontFamily: 'Nunito',
    fontWeight: '600',
    fontSize: 16,
    color: '#161F3D',
    marginLeft: 20,
    lineHeight: 22,
    marginTop: 20,
  },
  heart: {
    width: 30,
    height: 25,
    marginTop: 20,
    marginLeft: 20,
    marginBottom: 20,
  },
  heartText: {
    fontFamily: 'Nunito',
    fontWeight: '700',
    fontSize: 15,
    color: '#161F3D',
    marginTop: 20,
    marginLeft: 5,
  },
});
