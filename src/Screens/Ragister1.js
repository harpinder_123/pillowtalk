import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
  ImageBackground,
  ProgressBarAndroidBase,
  TextInput,
  FlatList,
} from 'react-native';
import {StatusBarDark} from '../Custom/CustomStatusBar';
import {Header, MainView} from '../Custom/CustomView';
import * as Progress from 'react-native-progress';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import LinearGradient from 'react-native-linear-gradient';
const {height, width} = Dimensions.get('window');

const Ragister1 = ({navigation}) => {
  const [select, setSelect] = useState(0);
  const [data, setData] = useState([
    {id: '1', title: 'Male', source: require('../images/male.png')},
    {id: '2', title: 'Female', source: require('../images/female.png')},
    {id: '3', title: 'Other', source: require('../images/other.png')},
  ]);
  return (
    <View style={{backgroundColor: '#fff', flex: 1}}>
      <StatusBarDark />
      <Header onPress={() => navigation.goBack()} />
      <ScrollView>
        <ImageBackground
          style={styles.image}
          source={require('../images/background.png')}>
          <Progress.Bar
            progress={0.4}
            width={330}
            color={'#3023AE'}
            style={{
              borderColor: '#D8D8D8',
              backgroundColor: '#D8D8D8',
              alignSelf: 'center',
              marginTop: 20,
            }}
          />
          <Text style={styles.basicText}>What is your{'\n'}gender ?</Text>
          <View>
            <FlatList
              numColumns={2}
              keyExtractor={item => item.id}
              data={data}
              renderItem={({item, index}) => (
                <TouchableOpacity
                  onPress={() => setSelect(index)}
                  style={{
                    width: width / 2 - 25,
                    paddingVertical: 20,
                    borderRadius: 10,
                    marginTop: 30,
                    marginHorizontal: 15,
                    backgroundColor: index == select ? '#3023AE' : '#ACB1C0',
                  }}>
                  <Image style={styles.containerImg} source={item.source} />
                  <Text style={styles.containerText}>{item.title}</Text>
                </TouchableOpacity>
              )}
            />
          </View>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              marginHorizontal: 20,
            }}>
            <Text style={styles.skip}>SKIP</Text>
            <TouchableOpacity
              activeOpacity={0.8}
              onPress={() => navigation.navigate('Ragister2')}
              style={styles.touch}>
              <Text style={styles.touchText}>NEXT</Text>
            </TouchableOpacity>
          </View>
        </ImageBackground>
      </ScrollView>
    </View>
  );
};

export default Ragister1;

const styles = StyleSheet.create({
  image: {
    width: '100%',
    height: '100%',
    resizeMode: 'contain',
    alignSelf: 'center',
  },
  basicText: {
    fontFamily: 'Nunito',
    fontSize: 34,
    fontWeight: '900',
    color: '#000000',
    marginTop: 60,
    textAlign: 'center',
  },
  containerImg: {
    width: 90,
    height: 90,
    alignSelf: 'center',
  },
  containerText: {
    fontFamily: 'Nunito',
    fontSize: 22,
    fontWeight: '600',
    color: '#fff',
    marginTop: 10,
    textAlign: 'center',
  },
  skip: {
    fontFamily: 'Nunito',
    fontSize: 20,
    fontWeight: '700',
    color: '#000000',
    marginTop: '30%',
  },
  touch: {
    width: '35%',
    height: 58,
    backgroundColor: '#3023AE',
    borderRadius: 30,
    marginTop: '25%',
    marginBottom: 20,
  },
  touchText: {
    fontFamily: 'Nunito',
    fontSize: 22,
    fontWeight: '700',
    color: '#fff',
    textAlign: 'center',
    marginTop: 12,
  },
});
