import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  TouchableOpacity,
  TextInput,
} from 'react-native';
import {StatusBarDark} from '../Custom/CustomStatusBar';
import {Header, MainView} from '../Custom/CustomView';
import {TextField} from 'react-native-material-textfield';
import LinearGradient from 'react-native-linear-gradient';

const Support = ({navigation}) => {
  const [state, setState] = useState({
    name: '',
    email: '',
    message: '',
    data: '',
    isLoading: false,
  });
  return (
    <View style={{backgroundColor: '#ffffff', flex: 1}}>
      <StatusBarDark />
      <Header onPress={() => navigation.goBack()} title={'Support'} />
      <ScrollView>
        <Image
          style={styles.topImage}
          source={require('../images/support1.png')}
        />
        <Text style={styles.subtext}>Need Some Help?</Text>
        <View style={styles.rowVieww}>
          <View
            style={{
              flexDirection: 'row',
              width: '90%',
              marginLeft: 20,
              marginVertical: 15,
            }}>
            <Image
              style={styles.siseImage}
              source={require('../images/email.png')}
            />
            <View style={{flexDirection: 'column', marginLeft: 10}}>
              <Text style={styles.emailText}>Email</Text>
              <Text style={styles.emailTextBlack}>info@pillowtalk.com</Text>
            </View>
          </View>
        </View>

        <Text style={styles.getText}>Get in Touch</Text>

        <TextField
          label="Name"
          style={styles.inputtxt}
          containerStyle={{
            marginHorizontal: 30,
            marginTop: 10,
            borderBottomWidth: 0,
          }}></TextField>
        <TextField
          label="Email"
          style={styles.inputtxt}
          containerStyle={{
            marginHorizontal: 30,
            marginTop: 10,
            borderBottomWidth: 0,
          }}></TextField>
        <TextField
          label="Complaint"
          secureTextEntry
          style={styles.inputtxt}
          containerStyle={{
            marginHorizontal: 30,
            marginTop: 10,
            borderBottomWidth: 0,
          }}></TextField>

        <TouchableOpacity
          activeOpacity={0.8}
          onPress={() => navigation.navigate('TabNavigator')}>
          <LinearGradient
            colors={['#C86DD7', '#3023AE']}
            start={{x: 0, y: 0}}
            end={{x: 1, y: 1}}
            style={styles.touchButton}>
            <Text style={styles.touchText}>SEND</Text>
          </LinearGradient>
        </TouchableOpacity>
      </ScrollView>
    </View>
  );
};

export default Support;

const styles = StyleSheet.create({
  touch: {
    padding: 15,
    marginHorizontal: 20,
    borderRadius: 25,
    backgroundColor: '#6CBDFF',
    marginTop: 30,
    marginBottom: 20,
  },
  touchtext: {
    fontFamily: 'Nunito',
    fontSize: 16,
    fontWeight: 'bold',
    alignSelf: 'center',
    color: '#ffffff',
  },
  input: {
    color: '#000',
    backgroundColor: 'pink',
    borderBottomColor: '#fff',
    borderBottomWidth: 0,
    tintColor: '#FFF',
  },
  getText: {
    fontFamily: 'Nunito',
    fontSize: 20,
    fontWeight: '700',
    color: '#000000',
    marginLeft: 25,
    marginTop: 40,
  },

  inputtxt: {
    marginTop: 30,
    alignSelf: 'center',
  },
  lineView: {
    backgroundColor: '#00000050',
    alignSelf: 'center',
    width: '90%',
    height: 1,
    marginVertical: 10,
  },
  getTouchText: {
    marginTop: 10,
    fontSize: 14,
    lineHeight: 18,
    fontFamily: 'Nunito',
    fontWeight: '400',
    color: '#7D7D7E',
    marginHorizontal: 20,
    marginBottom: 30,
  },
  emailTextBlack: {
    fontSize: 18,
    lineHeight: 24,
    fontFamily: 'Nunito',
    fontWeight: '500',
    color: '#000',
  },
  emailText: {
    fontSize: 16,
    lineHeight: 20,
    fontFamily: 'Nunito',
    fontWeight: '400',
    color: '#00000070',
  },
  nameText: {
    fontSize: 16,
    lineHeight: 20,
    fontFamily: 'Nunito',
    fontWeight: '400',
    color: '#00000070',
    marginLeft: 20,
  },
  textEmail: {
    marginTop: 18,
    fontSize: 16,
    lineHeight: 17,
    fontFamily: 'Nunito',
    fontWeight: '400',
    color: '#00000070',
    marginLeft: 20,
  },
  getTouchImage: {
    width: 30,
    height: 30,
    resizeMode: 'contain',
  },
  phoneImage: {
    width: 28,
    height: 28,
    marginTop: 10,
    resizeMode: 'contain',
  },
  siseImage: {
    width: 50,
    height: 50,
    // marginTop: 10,
    resizeMode: 'contain',
  },
  rowVieww: {
    marginTop: 30,
    width: '90%',
    alignSelf: 'center',
    backgroundColor: '#FFF',
    elevation: 5,
    marginBottom: 2,
    borderRadius: 10,
  },
  rowViewBottom: {
    marginTop: 10,
    width: '90%',
    alignSelf: 'center',
    backgroundColor: '#FFF',
    elevation: 5,
    marginBottom: 2,
    borderRadius: 10,
  },
  rowViewBottomEnd: {
    marginTop: 10,
    width: '90%',
    alignSelf: 'center',
    backgroundColor: '#FFF',
    elevation: 5,
    marginBottom: 40,
    borderRadius: 10,
  },
  topImage: {
    width: 150,
    height: 150,
    alignSelf: 'center',
    marginTop: 20,
  },
  subtext: {
    fontFamily: 'Nunito',
    fontWeight: '900',
    fontSize: 16,
    lineHeight: 25,
    color: '#000',
    alignSelf: 'center',
    marginTop: 10,
    marginHorizontal: 20,
  },
  touchButton: {
    padding: 10,
    marginHorizontal: 30,
    borderRadius: 23,
    marginTop: 50,
    marginBottom: 20,
  },
  touchText: {
    fontFamily: 'Nunito',
    fontWeight: '900',
    fontSize: 16,
    color: '#fff',
    textAlign: 'center',
  },
});
