import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
  useWindowDimensions,
  ImageBackgroundBase,
  ImageBackground,
  FlatList,
} from 'react-native';
import {StatusBarDark} from '../Custom/CustomStatusBar';
import {Header, MainView} from '../Custom/CustomView';
const {height} = Dimensions.get('window');

const Setting = ({navigation}) => {
  return (
    <View style={{backgroundColor: '#FFFFFF', flex: 1}}>
      <StatusBarDark />
      <Header onPress={() => navigation.goBack()} title={'Setting'} />
      <View style={styles.container}>
        <TouchableOpacity
          activeOpacity={0.7}
          style={styles.rowViewOffCss}
          onPress={() => navigation.navigate('About')}>
          <View style={{flexDirection: 'row'}}>
            <Image
              style={{width: 30, height: 30, resizeMode: 'contain'}}
              source={require('../images/about.png')}
            />
            <Text style={[styles.userNumberTextOffCss, {marginLeft: 15}]}>
              About Us
            </Text>
          </View>
          <Image
            style={{width: 10, height: 15, resizeMode: 'contain'}}
            source={require('../images/arrow.png')}
          />
        </TouchableOpacity>

        <TouchableOpacity
          activeOpacity={0.7}
          style={styles.rowViewOffCss}
          onPress={() => navigation.navigate('Privacy')}>
          <View style={{flexDirection: 'row'}}>
            <Image
              style={{width: 30, height: 30, resizeMode: 'contain'}}
              source={require('../images/privacy.png')}
            />
            <Text style={[styles.userNumberTextOffCss, {marginLeft: 15}]}>
              Privacy Policy
            </Text>
          </View>
          <Image
            style={{width: 10, height: 15, resizeMode: 'contain'}}
            source={require('../images/arrow.png')}
          />
        </TouchableOpacity>

        <TouchableOpacity
          activeOpacity={0.7}
          style={styles.rowViewOffCss}
          onPress={() => navigation.navigate('Terms')}>
          <View style={{flexDirection: 'row'}}>
            <Image
              style={{width: 30, height: 30, resizeMode: 'contain'}}
              source={require('../images/terms.png')}
            />
            <Text style={[styles.userNumberTextOffCss, {marginLeft: 15}]}>
              Terms & Conditions
            </Text>
          </View>
          <Image
            style={{width: 10, height: 15, resizeMode: 'contain'}}
            source={require('../images/arrow.png')}
          />
        </TouchableOpacity>

        <TouchableOpacity
          activeOpacity={0.7}
          style={styles.rowViewOffCss}
          onPress={() => navigation.navigate('Login')}>
          <View style={{flexDirection: 'row'}}>
            <Image
              style={{width: 30, height: 30, resizeMode: 'contain'}}
              source={require('../images/logout.png')}
            />
            <Text style={[styles.userNumberTextOffCss, {marginLeft: 15}]}>
              Logout
            </Text>
          </View>
          <Image
            style={{width: 10, height: 15, resizeMode: 'contain'}}
            source={require('../images/arrow.png')}
          />
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default Setting;

const styles = StyleSheet.create({
  image: {
    width: 66,
    height: 66,
    resizeMode: 'contain',
    marginTop: 40,
    marginLeft: 20,
  },
  text: {
    fontSize: 17,
    fontFamily: 'Nunito',
    fontWeight: 'bold',
    color: '#121213',
    marginLeft: 10,
    marginTop: 45,
  },
  subText: {
    fontSize: 14,
    fontFamily: 'Nunito',
    fontWeight: '600',
    color: '#7B87A5',
    marginLeft: '25%',
    marginTop: -30,
  },
  time: {
    fontSize: 12,
    fontFamily: 'Nunito',
    fontWeight: '600',
    color: '#7B87A5',
    marginLeft: 'auto',
    marginTop: 43,
    marginRight: 20,
  },
  container: {
    backgroundColor: '#fff',
    borderRadius: 8,
    padding: 10,
    marginHorizontal: 15,
    marginTop: 30,
    elevation: 5,
    marginBottom: 5,
  },
  rowViewOffCss: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: 15,
  },
  userNumberTextOffCss: {
    fontFamily: 'Nunito',
    fontSize: 15,
    fontWeight: '600',
    color: '#262626',
    marginTop: 2,
  },
  underLine: {
    height: 1,
    borderRadius: 5,
    backgroundColor: '#F5F5F5',
  },
});
