import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
  ImageBackground,
  ProgressBarAndroidBase,
  TextInput,
  FlatList,
} from 'react-native';
import {StatusBarDark} from '../Custom/CustomStatusBar';
import {Header, MainView} from '../Custom/CustomView';
import * as Progress from 'react-native-progress';
import LinearGradient from 'react-native-linear-gradient';
const {height, width} = Dimensions.get('window');

const Ragister5 = ({navigation}) => {
  const [select, setSelect] = useState(0);
  const [data, setData] = useState([
    {source: require('../images/upload.png')},
    {source: require('../images/upload.png')},
    {source: require('../images/upload.png')},
    {source: require('../images/upload.png')},
    {source: require('../images/upload.png')},
    {source: require('../images/upload.png')},
  ]);

  return (
    <View style={{backgroundColor: '#fff', flex: 1}}>
      <StatusBarDark />
      <Header onPress={() => navigation.goBack()} />
      <ScrollView>
        <ImageBackground
          style={styles.image}
          source={require('../images/background.png')}>
          <Progress.Bar
            progress={0.8}
            width={330}
            color={'#3023AE'}
            style={{
              borderColor: '#D8D8D8',
              backgroundColor: '#D8D8D8',
              alignSelf: 'center',
              marginTop: 20,
            }}
          />
          <Text style={styles.basicText}>My photo / Video</Text>

          <View
            style={{marginTop: 40, marginHorizontal: 15, alignItems: 'center'}}>
            <FlatList
              numColumns={3}
              keyExtractor={item => item.id}
              data={data}
              renderItem={({item, index}) => (
                <Image style={styles.flatImg} source={item.source} />
              )}
            />
          </View>
          <View>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                marginHorizontal: 20,
              }}>
              <Text style={styles.skip}>SKIP</Text>
              <TouchableOpacity
                activeOpacity={0.8}
                onPress={() => navigation.navigate('AccountCreated')}
                style={styles.touch}>
                <Text style={styles.touchText}>NEXT</Text>
              </TouchableOpacity>
            </View>
          </View>
        </ImageBackground>
      </ScrollView>
    </View>
  );
};

export default Ragister5;

const styles = StyleSheet.create({
  image: {
    width: '100%',
    height: '100%',
    resizeMode: 'contain',
    alignSelf: 'center',
  },
  basicText: {
    fontFamily: 'Nunito',
    fontSize: 34,
    fontWeight: '900',
    color: '#000000',
    marginTop: 60,
    textAlign: 'center',
  },
  flatImg: {
    width: 95,
    height: 150,
    // marginLeft: 20,
    margin: 5,
    alignSelf: 'center',
  },
  skip: {
    fontFamily: 'Nunito',
    fontSize: 20,
    fontWeight: '700',
    color: '#000000',
    marginTop: '50%',
  },
  touch: {
    width: '35%',
    height: 58,
    backgroundColor: '#3023AE',
    borderRadius: 30,
    marginTop: '45%',
    marginBottom: 20,
  },
  touchText: {
    fontFamily: 'Nunito',
    fontSize: 22,
    fontWeight: '700',
    color: '#fff',
    textAlign: 'center',
    marginTop: 12,
  },
});
