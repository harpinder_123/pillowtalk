import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
  ImageBackground,
  ProgressBarAndroidBase,
  TextInput,
} from 'react-native';
import {StatusBarDark} from '../Custom/CustomStatusBar';
import {Header, MainView} from '../Custom/CustomView';
import * as Progress from 'react-native-progress';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import LinearGradient from 'react-native-linear-gradient';
const {height} = Dimensions.get('window');

const Ragister = ({navigation}) => {
  return (
    <View style={{backgroundColor: '#fff', flex: 1}}>
      <KeyboardAwareScrollView>
        <StatusBarDark />
        <Header onPress={() => navigation.goBack()} />
        <ImageBackground
          style={styles.image}
          source={require('../images/background.png')}>
          <Progress.Bar
            progress={0.2}
            width={330}
            color={'#3023AE'}
            style={{
              borderColor: '#D8D8D8',
              backgroundColor: '#D8D8D8',
              alignSelf: 'center',
              marginTop: 20,
            }}
          />
          <Text style={styles.basicText}>Please enter basic{'\n'}info ?</Text>
          <TextInput
            // value={state.name}
            //   onChangeText={name => setState({...state, name})}
            style={styles.textInput}
            placeholder={'Name'}
          />
          <TextInput
            // value={state.name}
            //   onChangeText={name => setState({...state, name})}
            style={styles.textInput}
            placeholder={'Email Address'}
          />
          <TextInput
            // value={state.name}
            //   onChangeText={name => setState({...state, name})}
            style={styles.textInput}
            placeholder={'Mobile No.'}
            keyboardType={'number-pad'}
            maxLength={10}
          />
          <TextInput
            // value={state.name}
            //   onChangeText={name => setState({...state, name})}
            style={styles.textInput}
            placeholder={'Date of Birth'}
          />
          <Image
            style={{
              height: 18,
              width: 18,
              marginLeft: 'auto',
              marginHorizontal: 60,
              marginTop: -35,
            }}
            source={require('../images/calendar.png')}
          />
          <TouchableOpacity
            activeOpacity={0.8}
            onPress={() => navigation.navigate('Otp')}>
            <LinearGradient
              colors={['#C86DD7', '#3023AE']}
              start={{x: 0, y: 0}}
              end={{x: 1, y: 1}}
              style={styles.touchButton}>
              <Text style={styles.touchText}>SUBMIT</Text>
            </LinearGradient>
          </TouchableOpacity>
          <Text style={styles.bottomText}>
            Already have an account?{' '}
            <Text
              onPress={() => navigation.navigate('Login')}
              style={{color: '#3023AE'}}>
              Login Now
            </Text>
          </Text>
        </ImageBackground>
      </KeyboardAwareScrollView>
    </View>
  );
};

export default Ragister;

const styles = StyleSheet.create({
  image: {
    width: '100%',
    height: '100%',
    resizeMode: 'contain',
    alignSelf: 'center',
  },
  basicText: {
    fontFamily: 'Nunito',
    fontSize: 35,
    fontWeight: '900',
    color: '#000000',
    marginTop: 60,
    textAlign: 'center',
  },
  textInput: {
    borderRadius: 27,
    borderWidth: 1,
    padding: 10,
    paddingHorizontal: 20,
    marginHorizontal: 30,
    marginTop: 40,
    marginBottom: 0,
    fontSize: 12,
    fontFamily: 'Nunito',
    fontFamily: '500',
    backgroundColor: '#fff',
    borderColor: '#fff',
    elevation: 5,
  },
  touchButton: {
    padding: 10,
    marginHorizontal: 30,
    borderRadius: 23,
    marginTop: 60,
  },
  touchText: {
    fontFamily: 'Nunito',
    fontWeight: '900',
    fontSize: 16,
    color: '#fff',
    textAlign: 'center',
  },
  bottomText: {
    fontFamily: 'Nunito',
    fontWeight: '900',
    fontSize: 14,
    color: '#0C3637',
    marginTop: 10,
    textAlign: 'center',
    marginBottom: 20,
  },
});
