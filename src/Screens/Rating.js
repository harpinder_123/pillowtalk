import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
  useWindowDimensions,
  ImageBackgroundBase,
  ImageBackground,
  FlatList,
} from 'react-native';
import {StatusBarDark} from '../Custom/CustomStatusBar';
import {Header, MainView} from '../Custom/CustomView';
const {height} = Dimensions.get('window');

const Rating = ({navigation}) => {
  const [data, setData] = useState([
    {
      title: 'Sumit Kumar',
      subtitle: 'You sent a sticker',
      time: '5:30 PM',
      source: require('../images/male.png'),
    },
    {
      title: 'Deepak Kumar',
      subtitle: 'You sent a sticker',
      time: '5:30 PM',
      source: require('../images/male.png'),
    },
    {
      title: 'Rahul Kumar',
      subtitle: 'You sent a sticker',
      time: '5:30 PM',
      source: require('../images/male.png'),
    },
  ]);
  return (
    <View style={{backgroundColor: '#FFFFFF', flex: 1}}>
      <StatusBarDark />
      <Header onPress={() => navigation.goBack()} title={'Rating'} />
      <View style={styles.ratingBox}>
        <Image
          style={{width: 300, height: 150}}
          source={require('../images/rating.png')}
        />
      </View>
      <View>
        <FlatList
          numColumns={1}
          data={data}
          renderItem={({item, index}) => (
            <View style={styles.subBox}>
              <View style={{flexDirection: 'row'}}>
                <Image style={styles.image} source={item.source} />

                <Text style={styles.text}>{item.title}</Text>
              </View>
              <View style={{marginLeft: 45, marginTop: -5}}>
                <FlatList
                  horizontal
                  data={['', '', '', '', '']}
                  renderItem={(item, index) => {
                    return (
                      <Image
                        style={{
                          width: 11,
                          height: 11,
                          resizeMode: 'contain',
                        }}
                        source={require('../images/star.png')}
                      />
                    );
                  }}
                />
              </View>
              <Text style={styles.longText}>
                Suruchi Thank you so much{'\n'}
                you were so sweet all through the session{'\n'}
                lods of love and thanks once again
              </Text>
            </View>
          )}
        />
      </View>
    </View>
  );
};

export default Rating;

const styles = StyleSheet.create({
  ratingBox: {
    paddingHorizontal: 30,
    backgroundColor: '#fff',
    elevation: 5,
    marginTop: 40,
    marginHorizontal: 15,
    borderRadius: 12,
  },
  subBox: {
    paddingHorizontal: 30,
    backgroundColor: '#fff',
    elevation: 5,
    marginTop: 20,
    marginHorizontal: 15,
    borderRadius: 5,
  },
  image: {
    width: 35,
    height: 35,
    resizeMode: 'contain',
    marginTop: 10,
    // marginLeft: 20,
  },
  text: {
    fontSize: 16,
    fontFamily: 'Nunito',
    fontWeight: 'bold',
    color: '#121213',
    marginLeft: 10,
    marginTop: 10,
  },
  subText: {
    fontSize: 14,
    fontFamily: 'Nunito',
    fontWeight: '600',
    color: '#7B87A5',
    marginLeft: '25%',
    // marginTop: -30,
  },
  longText: {
    fontSize: 12,
    fontFamily: 'Nunito',
    fontWeight: '400',
    color: '#1D252D',
    marginBottom: 20,
    marginLeft: 45,
    marginTop: 5,
    lineHeight: 17,
  },
});
