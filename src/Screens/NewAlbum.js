import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
  ImageBackground,
} from 'react-native';
import {StatusBarDark} from '../Custom/CustomStatusBar';
import {Header, MainView} from '../Custom/CustomView';
const {height} = Dimensions.get('window');

const NewAlbum = ({navigation}) => {
  return (
    <View style={{backgroundColor: '#F7F7F7', flex: 1}}>
      <StatusBarDark />
      <Header onPress={() => navigation.goBack()} title={'New Album'} />
      <ScrollView contentContainerStyle={{flexGrow: 1}}>
        <View style={styles.container}>
          <Text style={styles.private}>Album Name </Text>

          <Text style={styles.email}>New Album</Text>

          <Text style={styles.private}>Album Price</Text>

          <Text style={styles.email}>250/min</Text>
        </View>

        <View style={styles.container}>
          <Text style={styles.private}>Description </Text>

          <Text style={styles.loremText}>
            Lorem Ipsum is simply dummy text of the{'\n'}printing and
            typesetting industry. Lorem{'\n'}Ipsum has been the industry's
            standard{'\n'}dummy text ever since the 1500s,{' '}
          </Text>
        </View>

        <View style={styles.container}>
          <Text style={styles.private}>Photos / Videos</Text>

          <View style={{flexDirection: 'row'}}>
            <Image
              style={{
                width: 70,
                height: 70,
                marginTop: 20,
                // marginLeft: 25,
                margin: 5,
                borderRadius: 10,
              }}
              source={require('../images/Rectangle1.png')}
            />
            <Image
              style={{
                width: 70,
                height: 70,
                marginTop: 20,
                // marginLeft: 15,
                margin: 5,
                borderRadius: 10,
              }}
              source={require('../images/Rectangle1.png')}
            />
            <ImageBackground
              imageStyle={{borderRadius: 10}}
              style={{
                width: 70,
                height: 70,
                marginTop: 20,
                // marginLeft: 15,
                margin: 5,
                borderRadius: 10,
              }}
              source={require('../images/Rectangle1.png')}>
              <Image
                style={{
                  width: 18,
                  height: 18,
                  alignSelf: 'center',
                  marginTop: 25,
                }}
                source={require('../images/play.png')}
              />
            </ImageBackground>
            <ImageBackground
              imageStyle={{borderRadius: 10}}
              style={{
                width: 70,
                height: 70,
                marginTop: 20,
                // marginLeft: 15,
                margin: 5,
              }}
              source={require('../images/Rectangle1.png')}>
              <Image
                style={{
                  width: 18,
                  height: 18,
                  alignSelf: 'center',
                  marginTop: 25,
                }}
                source={require('../images/play.png')}
              />
            </ImageBackground>
          </View>

          <View style={{flexDirection: 'row'}}>
            <Image
              style={{
                width: 70,
                height: 70,
                marginTop: 20,
                // marginLeft: 25,
                margin: 5,
                borderRadius: 10,
              }}
              source={require('../images/Rectangle1.png')}
            />
            <Image
              style={{
                width: 70,
                height: 70,
                marginTop: 20,
                // marginLeft: 15,
                margin: 5,
                borderRadius: 10,
              }}
              source={require('../images/Rectangle1.png')}
            />
            <ImageBackground
              imageStyle={{borderRadius: 10}}
              style={{
                width: 70,
                height: 70,
                marginTop: 20,
                // marginLeft: 15,
                margin: 5,
                borderRadius: 10,
              }}
              source={require('../images/Rectangle1.png')}>
              <Image
                style={{
                  width: 18,
                  height: 18,
                  alignSelf: 'center',
                  marginTop: 25,
                }}
                source={require('../images/play.png')}
              />
            </ImageBackground>
            <ImageBackground
              imageStyle={{borderRadius: 10}}
              style={{
                width: 70,
                height: 70,
                marginTop: 20,
                // marginLeft: 15,
                margin: 5,
              }}
              source={require('../images/Rectangle1.png')}>
              <Image
                style={{
                  width: 18,
                  height: 18,
                  alignSelf: 'center',
                  marginTop: 25,
                }}
                source={require('../images/play.png')}
              />
            </ImageBackground>
          </View>
        </View>
      </ScrollView>
    </View>
  );
};

export default NewAlbum;

const styles = StyleSheet.create({
  image: {
    width: 395,
    height: 280,
    alignSelf: 'center',
    resizeMode: 'contain',
  },
  cross: {
    width: 12,
    height: 22,
    marginTop: 20,
  },
  edit: {
    width: 5,
    height: 23,
    marginTop: 20,
    // marginLeft: 'auto',
    // marginHorizontal: 30,
  },
  contain: {
    backgroundColor: '#fff',
    borderRadius: 8,
    padding: 10,
    marginHorizontal: 15,
    marginTop: 40,
    elevation: 5,
  },
  container: {
    backgroundColor: '#fff',
    borderRadius: 8,
    padding: 10,
    marginHorizontal: 15,
    marginTop: 20,
    elevation: 5,
    marginBottom: 5,
  },
  private: {
    fontFamily: 'Nunito',
    fontSize: 16,
    fontWeight: '700',
    color: '#3023AE',
    marginTop: 10,
    marginHorizontal: 10,
  },
  centerImg: {
    width: 94,
    height: 94,
    alignSelf: 'center',
    marginTop: -60,
  },
  centerText: {
    fontFamily: 'Nunito',
    fontSize: 24,
    fontWeight: '600',
    color: '#262628',
    textAlign: 'center',
    marginTop: 5,
  },
  centerSubText: {
    fontFamily: 'Nunito',
    fontSize: 15,
    fontWeight: '600',
    color: '#ACB1C0',
    textAlign: 'center',
  },
  Line: {
    height: 1,
    borderRadius: 5,
    backgroundColor: '#F5F5F5',
    marginTop: 20,
    // marginHorizontal: 30,
    // marginLeft: 25,
  },
  underLine: {
    height: 1,
    borderRadius: 5,
    backgroundColor: '#F5F5F5',
  },
  bottomtext: {
    fontFamily: 'Nunito',
    fontSize: 12,
    fontWeight: '500',
    color: '#ACB1C0',
    marginTop: 10,
    marginHorizontal: 10,
    textAlign: 'center',
  },
  bottomsubtext: {
    fontFamily: 'Nunito',
    fontSize: 20,
    fontWeight: 'bold',
    color: '#000000',
    marginTop: 5,
    marginHorizontal: 10,
    textAlign: 'center',
  },
  rowViewOffCss: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: 15,
  },
  userNumberTextOffCss: {
    fontFamily: 'Nunito',
    fontSize: 15,
    fontWeight: '600',
    color: '#262626',
  },
  loremText: {
    fontFamily: 'Nunito',
    fontSize: 15,
    fontWeight: '600',
    color: '#1E1F20',
    lineHeight: 27,
    marginLeft: 10,
    marginTop: 10,
  },
  email: {
    fontFamily: 'Nunito',
    fontSize: 16,
    fontWeight: '600',
    color: '#1E1F20',
    marginLeft: 10,
  },
});
