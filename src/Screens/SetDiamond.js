import React, {useState} from 'react';
import {
  Dimensions,
  Image,
  Modal,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  TextInput,
} from 'react-native';

const {height, width} = Dimensions.get('window');
const SetDiamond = ({navigation}) => {
  const [modalOpen, setModalOpen] = useState(true);
  const [checked, setChecked] = useState('first');

  return (
    <Modal visible={modalOpen} transparent={true}>
      <View style={styles.modal_View}>
        <View style={styles.mdtop}>
          <View style={styles.mdtop_1}>
            <Text style={styles.mdTopText}>Set Diamond</Text>
            <TouchableOpacity
              activeOpacity={0.8}
              onPress={() => navigation.navigate('TabNavigator')}>
              <Image
                style={{width: 30, height: 30}}
                source={require('../images/close2.png')}
              />
            </TouchableOpacity>
          </View>

          <TextLabel title={'Call Diamond'} />
          <TextInput
            //   value={state.name}
            //   onChangeText={name => setState({...state, name})}
            style={styles.textInput}
            placeholder={'250/min'}
            keyboardType={'phone-pad'}
            maxLength={4}
          />
          <TextLabel title={'Video Call Diamond'} />
          <TextInput
            //   value={state.name}
            //   onChangeText={name => setState({...state, name})}
            style={styles.textInput}
            placeholder={'700/min'}
            keyboardType={'phone-pad'}
            maxLength={4}
          />
          <TouchableOpacity
            activeOpacity={0.8}
            onPress={() => navigation.navigate('Success')}
            style={styles.touch}>
            <Text style={styles.touchText}>UPDATE</Text>
          </TouchableOpacity>
        </View>
      </View>
    </Modal>
  );
};

export default SetDiamond;
const TextLabel = ({title}) => <Text style={styles.textLabel}>{title}</Text>;

const styles = StyleSheet.create({
  modal_View: {
    backgroundColor: '#000000aa',
    flex: 1,
  },
  mdtop: {
    backgroundColor: '#FFFFFF',
    marginTop: height / 3,
    marginHorizontal: 20,
    borderRadius: 20,
  },
  mdtop_1: {
    backgroundColor: '#3023AE',
    padding: 10,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  mdmiddle: {
    backgroundColor: '#ED6E1E',
    padding: 10,
    width: width / 3,
    borderRadius: 10,
    marginTop: 20,
  },
  mdImage: {
    width: 95,
    height: 42,
    resizeMode: 'contain',
    marginTop: 10,
  },
  mdText: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 13,
    fontWeight: 'bold',
    color: '#FFFFFF',
    alignSelf: 'center',
    marginTop: 5,
  },
  mdmiddle_1: {
    backgroundColor: '#00000030',
    padding: 10,
    width: width / 2.5,
    borderRadius: 10,
    marginTop: 20,
  },
  mdImage_1: {
    width: 40,
    height: 48,
    resizeMode: 'contain',
    alignSelf: 'center',
    marginTop: 10,
  },
  mdmiddleView: {
    flexDirection: 'row',
    marginHorizontal: 20,
    justifyContent: 'space-around',
  },
  mdTopText: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 20,
    fontWeight: 'bold',
    color: '#FFFFFF',
    marginLeft: 12,
  },
  mdbottomView: {
    backgroundColor: '#ED6E1E',
    borderRadius: 20,
    padding: 10,
    marginTop: 40,
    marginHorizontal: 25,
    marginBottom: 20,
  },
  mdBottomText: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: 'bold',
    fontSize: 16,
    color: '#FFFFFF',
    textAlign: 'center',
  },
  topv4_Style: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 20,
    marginHorizontal: 20,
    justifyContent: 'space-between',
  },
  text2_Style: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 20,
    color: '#9F9F9F',
    fontWeight: 'bold',
  },
  subtext2_Style: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 14,
    color: '#9F9F9F',
    fontWeight: 'bold',
  },
  line: {
    borderColor: '#00000020',
    borderWidth: 0.5,
    marginTop: 15,
  },
  textLabel: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: '900',
    fontSize: 14,
    color: '#7A7A7A',
    marginHorizontal: 20,
    marginTop: 10,
    marginBottom: 0,
  },
  textInput: {
    borderRadius: 10,
    borderWidth: 1,
    padding: 4,
    paddingHorizontal: 10,
    marginHorizontal: 20,
    marginTop: 5,
    marginBottom: 0,
    borderColor: '#F6F4F4',
    fontSize: 12,
    fontFamily: 'Aviner-Medium',
    fontFamily: '500',
    backgroundColor: '#F6F4F4',
  },
  touch: {
    width: '40%',
    height: 45,
    backgroundColor: '#3023AE',
    borderRadius: 30,
    marginTop: 30,
    alignSelf: 'center',
    marginBottom: 20,
  },
  touchText: {
    fontFamily: 'Nunito',
    fontSize: 16,
    fontWeight: '700',
    color: '#fff',
    textAlign: 'center',
    marginTop: 12,
  },
});
