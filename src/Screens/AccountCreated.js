import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
  Modal,
  ImageBackground,
} from 'react-native';
import {StatusBarDark} from '../Custom/CustomStatusBar';
import Dialog, {DialogContent} from 'react-native-popup-dialog';
const {height} = Dimensions.get('window');

const AccountCreated = ({navigation}) => {
  const [modalOpen, setModalOpen] = useState(true);

  const handlePaymentSuccessful = () => {
    navigation.replace('');
    setVisible(false);
  };

  return (
    <View style={{backgroundColor: '#FFFFFF', flex: 1}}>
      <StatusBarDark />

      <Modal visible={modalOpen} transparent={true}>
        <View style={styles.modal_View}>
          <View style={styles.mdtop}>
            <Image
              style={{
                width: 70,
                height: 70,
                alignSelf: 'center',
                marginTop: 40,
              }}
              source={require('../images/smile.png')}
            />
            <Text style={styles.text}>Success</Text>

            <Text style={styles.subText}>
              congratulations !{'\n'}
              Your account has been created
            </Text>

            <TouchableOpacity
              activeOpacity={0.8}
              style={styles.mdbottomView}
              onPress={() => navigation.navigate('TabNavigator')}>
              <Text style={styles.mdBottomText}>CONTINUE</Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    </View>
  );
};

export default AccountCreated;

const styles = StyleSheet.create({
  modal_View: {
    backgroundColor: '#000000aa',
    flex: 1,
  },
  mdtop: {
    backgroundColor: '#FFFFFF',
    marginTop: height / 3.5,
    marginHorizontal: 20,
    borderRadius: 20,
  },
  text: {
    fontFamily: 'Nunito',
    fontWeight: '600',
    fontSize: 28,
    color: '#363636',
    textAlign: 'center',
    marginTop: 40,
  },
  subText: {
    fontFamily: 'Nunito',
    fontWeight: '600',
    fontSize: 20,
    color: '#ACB1C0',
    textAlign: 'center',
    marginTop: 10,
  },
  mdbottomView: {
    backgroundColor: '#3023AE',
    borderRadius: 20,
    paddingVertical: 10,
    marginTop: 40,
    // marginHorizontal: 25,
    width: '45%',
    // height: 40,
    marginBottom: 20,
    alignSelf: 'center',
  },
  mdBottomText: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: 'bold',
    fontSize: 16,
    color: '#FFFFFF',
    textAlign: 'center',
  },
});
