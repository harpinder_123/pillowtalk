import React, {useEffect, useState, useRef} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
  useWindowDimensions,
  ImageBackgroundBase,
  ImageBackground,
  FlatList,
} from 'react-native';
import {StatusBarDark} from '../Custom/CustomStatusBar';
import {Header, MainView} from '../Custom/CustomView';
import {TabView, SceneMap, TabBar} from 'react-native-tab-view';
import RBSheet from 'react-native-raw-bottom-sheet';
import {RadioButton} from 'react-native-paper';
import {useNavigation} from '@react-navigation/native';
const {height, width} = Dimensions.get('window');

const All = () => {
  const navigation = useNavigation();
  const [data, setData] = useState([
    {
      id: '1',
      title: 'Belle Benson, 28',
      source: require('../images/images.png'),
    },
    {id: '2', title: 'Ruby Diaz, 33', source: require('../images/images.png')},
    {
      id: '3',
      title: 'Martha Rojas, 24',
      source: require('../images/images.png'),
    },
    {id: '4', title: 'Ruby Diaz, 33', source: require('../images/images.png')},
    {
      id: '5',
      title: 'Belle Benson, 28',
      source: require('../images/images.png'),
    },
    {id: '6', title: 'Ruby Diaz, 28', source: require('../images/images.png')},
  ]);
  return (
    <View style={{flex: 1, backgroundColor: '#fff'}}>
      <FlatList
        numColumns={2}
        keyExtractor={item => item.id}
        data={data}
        renderItem={({item, index}) => (
          <TouchableOpacity
            activeOpacity={0.8}
            onPress={() => navigation.navigate('OtherProfile')}>
            <ImageBackground
              style={styles.images}
              imageStyle={{borderRadius: 10}}
              source={item.source}>
              <View style={{flexDirection: 'row'}}>
                <Image
                  style={styles.starImg}
                  source={require('../images/star.png')}
                />
                <Text style={styles.starText}>4.5</Text>
              </View>
              <Text style={styles.text}>{item.title}</Text>
              <View style={{flexDirection: 'row'}}>
                <Text style={styles.subText}>1.5 Km away</Text>
                <Image
                  style={{
                    width: 13,
                    height: 13,
                    resizeMode: 'contain',
                    marginLeft: 'auto',
                    marginRight: 5,
                    marginTop: 5,
                  }}
                  source={require('../images/photo-camera.png')}
                />
                <Text
                  style={{
                    color: '#fff',
                    fontSize: 12,
                    marginTop: 3,
                    marginRight: 10,
                  }}>
                  35
                </Text>
              </View>
            </ImageBackground>
          </TouchableOpacity>
        )}
      />
    </View>
  );
};

const Online = () => <View style={{flex: 1, backgroundColor: '#fff'}} />;

const NewlyJoined = () => <View style={{flex: 1, backgroundColor: '#fff'}} />;

const LikedYou = () => <View style={{flex: 1, backgroundColor: '#fff'}} />;

const renderScene = SceneMap({
  first: All,
  second: Online,
  third: NewlyJoined,
  fourth: LikedYou,
});

const Explore = ({navigation}) => {
  const refRBSheet = useRef();
  const [checked, setChecked] = useState('first');
  const layout = useWindowDimensions();
  const [index, setIndex] = React.useState(0);
  const [routes] = React.useState([
    {key: 'first', title: 'All'},
    {key: 'second', title: 'Online'},
    {key: 'third', title: 'Newly Joined'},
    {key: 'fourth', title: 'Liked You'},
  ]);

  return (
    <View style={{backgroundColor: '#FFFFFF', flex: 1}}>
      <StatusBarDark />
      <Header onPress={() => navigation.goBack()} title={'Explore'} />
      <TouchableOpacity
        style={{marginLeft: 'auto', marginRight: 30, marginTop: -35}}
        onPress={() => refRBSheet.current.open()}>
        <Image style={styles.image} source={require('../images/filter.png')} />
      </TouchableOpacity>
      <View>
        <ScrollView>
          <RBSheet
            ref={refRBSheet}
            closeOnDragDown={true}
            closeOnPressMask={false}
            height={850}
            openDuration={250}
            customStyles={{
              // wrapper: {
              //   backgroundColor: '#0000004d',
              // },
              draggableIcon: {
                backgroundColor: '#D8D8D8',
              },
              container: {
                backgroundColor: '#fff',
                //   borderRadius: 35,
                borderBottomEndRadius: 0,
                borderBottomLeftRadius: 0,
              },
            }}>
            {/* <YourOwnComponent /> */}
            <View style={{flexDirection: 'row'}}>
              <TouchableOpacity onPress={() => navigation.navigate('Explore')}>
                <Image
                  style={styles.filterimage}
                  source={require('../images/close.png')}
                />
              </TouchableOpacity>
              <Text style={styles.filtertext}>Filters</Text>
              <Text style={styles.clearAll}>CLEAR ALL</Text>
            </View>
            <View style={styles.underLine} />
            <View style={{flexDirection: 'row'}}>
              <View style={styles.back}>
                <View style={styles.textColor}>
                  <Text style={styles.filtersubtext}>Age</Text>
                  <View style={styles.Line} />
                  <Text style={styles.filtersub2text}>Location</Text>
                  <View style={styles.Line} />
                  <Text style={styles.filtersub2text}>Rating</Text>
                  <View style={styles.Line} />
                  <Text style={styles.filtersub2text}>Interest</Text>
                  <View style={styles.Line} />
                  <Text style={styles.filtersub2text}>Gender</Text>
                  <View style={styles.Line} />
                </View>
              </View>
              <View style={{flexDirection: 'column'}}>
                <Text style={{marginLeft: 30, marginTop: 15}}>Show Age By</Text>
                <View style={{flexDirection: 'row'}}>
                  <View style={{marginHorizontal: 20, marginTop: 30}}>
                    <RadioButton
                      value="first"
                      status={checked === 'first' ? 'checked' : 'unchecked'}
                      onPress={() => setChecked('first')}
                      uncheckedColor={'#263238'}
                      color={'#3023AE'}
                    />
                  </View>
                  <Text style={styles.radioText}>18 Years</Text>
                </View>
                <View style={{flexDirection: 'row'}}>
                  <View style={{marginHorizontal: 20, marginTop: 15}}>
                    <RadioButton
                      value="second"
                      status={checked === 'second' ? 'checked' : 'unchecked'}
                      onPress={() => setChecked('second')}
                      uncheckedColor={'#263238'}
                      color={'#3023AE'}
                    />
                  </View>
                  <Text style={styles.radio2Text}>20 Years</Text>
                </View>
                <View style={{flexDirection: 'row'}}>
                  <View style={{marginHorizontal: 20, marginTop: 15}}>
                    <RadioButton
                      value="third"
                      status={checked === 'third' ? 'checked' : 'unchecked'}
                      onPress={() => setChecked('third')}
                      uncheckedColor={'#263238'}
                      color={'#3023AE'}
                    />
                  </View>
                  <Text style={styles.radio2Text}>22 Years</Text>
                </View>
                <View style={{flexDirection: 'row'}}>
                  <View style={{marginHorizontal: 20, marginTop: 15}}>
                    <RadioButton
                      value="fourth"
                      status={checked === 'fourth' ? 'checked' : 'unchecked'}
                      onPress={() => setChecked('fourth')}
                      uncheckedColor={'#263238'}
                      color={'#3023AE'}
                    />
                  </View>
                  <Text style={styles.radio2Text}>24 Years</Text>
                </View>
                <View style={{flexDirection: 'row'}}>
                  <View style={{marginHorizontal: 20, marginTop: 15}}>
                    <RadioButton
                      value="fivth"
                      status={checked === 'fivth' ? 'checked' : 'unchecked'}
                      onPress={() => setChecked('fivth')}
                      uncheckedColor={'#263238'}
                      color={'#3023AE'}
                    />
                  </View>
                  <Text style={styles.radio2Text}>26 Years</Text>
                </View>
              </View>
            </View>

            <TouchableOpacity
              activeOpacity={0.8}
              style={styles.filtertouch2}
              onPress={() => navigation.navigate('Explore')}>
              <Text style={styles.filtertouch2text}>Apply</Text>
            </TouchableOpacity>
          </RBSheet>
        </ScrollView>
      </View>
      <TabView
        navigationState={{index, routes}}
        renderScene={renderScene}
        onIndexChange={setIndex}
        initialLayout={{width: layout.width}}
        renderTabBar={props => (
          <TabBar
            style={styles.style}
            labelStyle={styles.labelStyle}
            scrollEnabled={false}
            activeColor={'#3023AE'}
            inactiveColor={'#ACB1C0'}
            inactiveOpacity={0.5}
            {...props}
            indicatorStyle={styles.indicatorStyle}
          />
        )}
      />
    </View>
  );
};

export default Explore;

const styles = StyleSheet.create({
  image: {
    width: 18,
    height: 18,
    resizeMode: 'contain',
  },
  style: {backgroundColor: 'white', marginTop: 20, elevation: 0},
  labelStyle: {
    fontSize: 16,
    fontFamily: 'Nunito',
    fontWeight: 'bold',
    color: '#ACB1C0',
  },
  indicatorStyle: {
    backgroundColor: '#3023AE',
    height: 3,
    width: '15%',
    marginHorizontal: 20,
  },
  starImg: {
    width: 13,
    height: 13,
    marginTop: 10,
    marginLeft: 10,
  },
  starText: {
    fontSize: 16,
    fontFamily: 'Nunito',
    fontWeight: 'bold',
    color: '#fff',
    marginLeft: 7,
    marginTop: 5,
  },
  clearAll: {
    fontSize: 13,
    fontFamily: 'Nunito',
    fontWeight: '700',
    color: '#3023AE',
    marginLeft: 'auto',
    marginTop: 5,
    marginHorizontal: 15,
  },
  images: {
    width: width / 2 - 25,
    height: 180,
    marginTop: 20,
    borderRadius: 10,
    // margin: 5,
    marginHorizontal: 15,
  },
  text: {
    fontSize: 14,
    fontFamily: 'Nunito',
    fontWeight: 'bold',
    color: '#fff',
    marginLeft: 10,
    marginTop: '60%',
  },
  subText: {
    fontSize: 12,
    fontFamily: 'Nunito',
    fontWeight: '600',
    color: '#fff',
    marginLeft: 10,
    marginTop: 5,
  },
  filtertext: {
    fontFamily: 'AvenirLTStd-Heavy',
    fontSize: 20,
    fontWeight: 'bold',
    color: '#263238',
    marginLeft: 20,
  },
  filterimage: {
    width: 16,
    height: 16,
    marginLeft: 30,
    marginTop: 5,
  },
  filtersubtext: {
    color: '#000',
    fontSize: 16,
    fontWeight: 'bold',
    marginTop: 15,
    marginHorizontal: 15,
  },
  filtersub2text: {
    color: '#8D92A3',
    fontSize: 16,
    fontWeight: '500',
    marginTop: 20,
    marginHorizontal: 15,
  },
  radioText: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 18,
    fontWeight: '500',
    color: '#000521',
    marginTop: 33,
    marginLeft: -15,
  },
  radio2Text: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 18,
    fontWeight: '500',
    color: '#8D92A3',
    marginTop: 18,
    marginLeft: -15,
  },
  filtertouch: {
    width: 145,
    height: 50,
    borderRadius: 5,
    backgroundColor: '#6D7278',
    marginTop: '5%',
    elevation: 15,
  },
  filtertouchtext: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 17,
    fontWeight: '500',
    alignSelf: 'center',
    color: '#ffffff',
    marginTop: 12,
  },
  filtertouch2: {
    width: 235,
    height: 50,
    backgroundColor: '#0253B3',
    marginTop: -55,
    elevation: 15,
    marginLeft: 'auto',
  },
  filtertouch2text: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 17,
    fontWeight: '500',
    textAlign: 'center',
    color: '#ffffff',
    marginTop: 12,
  },
  back: {
    width: 158,
    height: 704,
    backgroundColor: '#F3F4F6',
    // marginTop: 20,
  },
  textColor: {
    width: 158,
    height: 50,
    backgroundColor: '#fff',
    // marginTop: 20,
    borderColor: '#EAECEF',
    borderWidth: 1,
  },
  underLine: {
    height: 1,
    borderRadius: 5,
    backgroundColor: '#EAECEF',
    marginTop: 20,
  },
  Line: {
    height: 1,
    borderRadius: 5,
    backgroundColor: '#EAECEF',
    marginTop: 10,
  },
});
