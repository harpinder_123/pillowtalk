import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
  useWindowDimensions,
  ImageBackgroundBase,
  ImageBackground,
  FlatList,
} from 'react-native';
import {StatusBarDark} from '../Custom/CustomStatusBar';
import {Header, MainView} from '../Custom/CustomView';
import LinearGradient from 'react-native-linear-gradient';
const {height} = Dimensions.get('window');

const MyEarning = ({navigation}) => {
  return (
    <View style={{backgroundColor: '#FFFFFF', flex: 1}}>
      <StatusBarDark />
      <Header onPress={() => navigation.goBack()} title={'My Earning'} />
      <ScrollView>
        <View style={styles.containBox}>
          <Image
            style={styles.image}
            source={require('../images/diamond.png')}
          />
          <Text style={styles.rateText}>450</Text>
          <Text style={styles.text}>Total Diamond </Text>
        </View>
        <Text style={styles.noteText}>Note:-</Text>
        <Text style={styles.noteSubText}>
          You need min 100 diamonds to request{'\n'}a payout.
        </Text>
        <TouchableOpacity
          activeOpacity={0.8}
          onPress={() => navigation.navigate('RequestPayout')}>
          <LinearGradient
            colors={['#C86DD7', '#3023AE']}
            start={{x: 0, y: 0}}
            end={{x: 1, y: 1}}
            style={styles.touchButton}>
            <Text style={styles.touchText}>Request Payout</Text>
          </LinearGradient>
        </TouchableOpacity>
      </ScrollView>
    </View>
  );
};

export default MyEarning;

const styles = StyleSheet.create({
  containBox: {
    padding: 10,
    marginHorizontal: 25,
    backgroundColor: '#3023AE',
    borderRadius: 12,
    marginTop: 30,
  },
  image: {
    width: 60,
    height: 45,
    alignSelf: 'center',
    marginTop: 20,
  },
  rateText: {
    fontFamily: 'Nunito',
    fontSize: 36,
    fontWeight: '700',
    color: '#ffffff',
    alignSelf: 'center',
    marginTop: 5,
  },
  text: {
    fontFamily: 'Nunito',
    fontSize: 22,
    fontWeight: '700',
    color: '#ffffff',
    alignSelf: 'center',
    marginTop: 5,
    marginBottom: 10,
  },
  noteText: {
    fontFamily: 'Nunito',
    fontSize: 16,
    fontWeight: '600',
    color: '#FF0000',
    marginLeft: 25,
    marginTop: 15,
  },
  noteSubText: {
    fontFamily: 'Nunito',
    fontSize: 18,
    fontWeight: '600',
    color: '#747A8D',
    marginLeft: 25,
    marginTop: 5,
  },
  touchButton: {
    padding: 10,
    marginHorizontal: 30,
    borderRadius: 23,
    marginTop: '90%',
    marginBottom: 20,
  },
  touchText: {
    fontFamily: 'Nunito',
    fontWeight: '900',
    fontSize: 16,
    color: '#fff',
    textAlign: 'center',
  },
  recharge: {
    fontFamily: 'Nunito',
    fontSize: 22,
    fontWeight: '700',
    color: '#000000',
    marginTop: 20,
    marginLeft: 25,
  },
  diamondBox: {
    width: '30%',
    height: 160,
    backgroundColor: '#fff',
    elevation: 5,
    borderRadius: 15,
    margin: 7,
  },
  diamondImage: {
    width: 48,
    height: 36,
    alignSelf: 'center',
    marginTop: 20,
  },
  diamondRateText: {
    fontFamily: 'Nunito',
    fontSize: 14,
    fontWeight: '600',
    color: '#ACB1C0',
    marginTop: 10,
    textAlign: 'center',
  },
  diamondText: {
    fontFamily: 'Nunito',
    fontSize: 16,
    fontWeight: '600',
    color: '#ffffff',
    paddingVertical: 5,
    textAlign: 'center',
  },
});
