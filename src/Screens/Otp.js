import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import {StatusBarDark} from '../Custom/CustomStatusBar';
import OTPInputView from '@twotalltotems/react-native-otp-input';
import LinearGradient from 'react-native-linear-gradient';
const {height} = Dimensions.get('window');

const Otp = ({navigation}) => {
  const [otp, setOtp] = useState('');
  return (
    <View style={{backgroundColor: '#FFFFFF', flex: 1}}>
      <StatusBarDark />
      <Text style={styles.text}>Phone Verification</Text>
      <Text style={styles.subText}>Enter your OTP code here</Text>
      <OTPInputView
        style={styles.otpInput}
        pinCount={4}
        // code={state.otp}
        onCodeChanged={text => {
          setOtp(text.replace(/[^0-9]/g, ''));
          console.log('--------otp: ', +text);
        }}
        autoFocusOnLoad
        codeInputFieldStyle={styles.underlineStyleBase}
        codeInputHighlightStyle={styles.underlineStyleHighLighted}
      />
      <Text style={styles.bottomText}>Didn’t you received any code?</Text>
      <Text style={styles.bottomSubtext}>Resend a new code.</Text>
      <TouchableOpacity
        activeOpacity={0.8}
        onPress={() => navigation.navigate('Ragister1')}>
        <LinearGradient
          colors={['#C86DD7', '#3023AE']}
          start={{x: 0, y: 0}}
          end={{x: 1, y: 1}}
          style={styles.touchButton}>
          <Text style={styles.touchText}>VERIFY</Text>
        </LinearGradient>
      </TouchableOpacity>
    </View>
  );
};

export default Otp;

const styles = StyleSheet.create({
  image: {
    marginTop: height / 3,
    width: 219,
    height: 232,
    resizeMode: 'contain',
    alignSelf: 'center',
  },
  text: {
    fontFamily: 'Nunito',
    fontSize: 36,
    fontWeight: '600',
    color: '#000000',
    marginTop: height / 6,
    marginLeft: 30,
  },
  subText: {
    fontFamily: 'Nunito',
    fontSize: 17,
    fontWeight: '600',
    color: '#ACB1C0',
    marginVertical: 10,
    marginLeft: 30,
  },
  otpInput: {
    height: 60,
    marginTop: 60,
    marginHorizontal: 30,
  },
  underlineStyleBase: {
    width: 60,
    height: 60,
    borderWidth: 1,
    borderColor: '#F1F2F6',
    backgroundColor: '#F1F2F6',
    borderRadius: 5,
    fontFamily: 'Avenir-Heavy',
    fontSize: 24,
    fontWeight: 'bold',
    color: '#000000',
  },
  underlineStyleHighLighted: {
    width: 60,
    height: 60,
    borderRadius: 5,
    fontFamily: 'Avenir-Heavy',
    fontSize: 24,
    fontWeight: 'bold',
    color: 'white',
    backgroundColor: '#3023AE',
  },
  bottomText: {
    fontFamily: 'Nunito',
    fontWeight: '500',
    fontSize: 15,
    color: '#606E87',
    opacity: 0.6,
    marginTop: 30,
    textAlign: 'center',
  },
  bottomSubtext: {
    fontFamily: 'Nunito',
    fontWeight: 'bold',
    fontSize: 17,
    color: '#3023AE',
    textAlign: 'center',
    marginTop: 5,
  },
  touchButton: {
    padding: 10,
    marginHorizontal: 50,
    borderRadius: 23,
    marginTop: 60,
  },
  touchText: {
    fontFamily: 'Nunito',
    fontWeight: '900',
    fontSize: 16,
    color: '#fff',
    textAlign: 'center',
  },
});
