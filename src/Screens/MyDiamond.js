import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
  useWindowDimensions,
  ImageBackgroundBase,
  ImageBackground,
  FlatList,
} from 'react-native';
import {StatusBarDark} from '../Custom/CustomStatusBar';
import {Header, MainView} from '../Custom/CustomView';
const {height} = Dimensions.get('window');

const MyDiamond = ({navigation}) => {
  const [data, setData] = useState([
    {source: require('../images/diamond.png'), title: '₹89.00'},
    {source: require('../images/diamond.png'), title: '₹400.00'},
    {source: require('../images/diamond.png'), title: '₹89.00'},
    {source: require('../images/diamond.png'), title: '₹89.00'},
    {source: require('../images/diamond.png'), title: '₹89.00'},
    {source: require('../images/diamond.png'), title: '₹89.00'},
  ]);
  return (
    <View style={{backgroundColor: '#FFFFFF', flex: 1}}>
      <StatusBarDark />
      <Header onPress={() => navigation.goBack()} title={'My Diamond'} />
      <ScrollView>
        <View style={styles.containBox}>
          <Image
            style={styles.image}
            source={require('../images/diamond.png')}
          />
          <Text style={styles.rateText}>450</Text>
          <Text style={styles.text}>Total Diamond </Text>
        </View>
        <Text style={styles.recharge}>Recharge</Text>
        <View style={{marginHorizontal: 15, marginTop: 20}}>
          <FlatList
            numColumns={3}
            keyExtractor={item => item.id}
            data={data}
            renderItem={({item, index}) => (
              <View style={styles.diamondBox}>
                <Image style={styles.diamondImage} source={item.source} />
                <Text style={styles.diamondRateText}>1590{'\n'}diamond</Text>
                <View
                  style={{
                    backgroundColor: '#3023AE',
                    paddingHorizontal: 10,
                    borderRadius: 12,
                    marginTop: 28,
                    borderTopRightRadius: 0,
                    borderTopLeftRadius: 0,
                  }}>
                  <Text style={styles.diamondText}>{item.title}</Text>
                </View>
              </View>
            )}
          />
        </View>
      </ScrollView>
    </View>
  );
};

export default MyDiamond;

const styles = StyleSheet.create({
  containBox: {
    padding: 10,
    marginHorizontal: 25,
    backgroundColor: '#3023AE',
    borderRadius: 12,
    marginTop: 30,
  },
  image: {
    width: 60,
    height: 45,
    alignSelf: 'center',
    marginTop: 20,
  },
  rateText: {
    fontFamily: 'Nunito',
    fontSize: 36,
    fontWeight: '700',
    color: '#ffffff',
    alignSelf: 'center',
    marginTop: 5,
  },
  text: {
    fontFamily: 'Nunito',
    fontSize: 22,
    fontWeight: '700',
    color: '#ffffff',
    alignSelf: 'center',
    marginTop: 5,
    marginBottom: 10,
  },
  recharge: {
    fontFamily: 'Nunito',
    fontSize: 22,
    fontWeight: '700',
    color: '#000000',
    marginTop: 20,
    marginLeft: 25,
  },
  diamondBox: {
    width: '30%',
    height: 160,
    backgroundColor: '#fff',
    elevation: 5,
    borderRadius: 15,
    margin: 7,
  },
  diamondImage: {
    width: 48,
    height: 36,
    alignSelf: 'center',
    marginTop: 20,
  },
  diamondRateText: {
    fontFamily: 'Nunito',
    fontSize: 14,
    fontWeight: '600',
    color: '#ACB1C0',
    marginTop: 10,
    textAlign: 'center',
  },
  diamondText: {
    fontFamily: 'Nunito',
    fontSize: 16,
    fontWeight: '600',
    color: '#ffffff',
    paddingVertical: 5,
    textAlign: 'center',
  },
});
