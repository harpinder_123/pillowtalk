import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
  TextInput,
} from 'react-native';
import {StatusBarDark} from '../Custom/CustomStatusBar';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import LinearGradient from 'react-native-linear-gradient';
const {height} = Dimensions.get('window');

const Login = ({navigation}) => {
  return (
    <View style={{backgroundColor: '#FFFFFF', flex: 1}}>
      <KeyboardAwareScrollView>
        <StatusBarDark />
        <View style={{flexDirection: 'row'}}>
          <Text style={styles.text}>
            Login to{'\n'}a pillow{'\n'}talk
          </Text>
          <Image style={styles.image} source={require('../images/boy.png')} />
        </View>
        <TextLabel title={'Mobile No.'} />
        <TextInput
          // value={state.name}
          //   onChangeText={name => setState({...state, name})}
          style={styles.textInput}
          placeholder={'Mobile No.'}
          keyboardType={'number-pad'}
          maxLength={10}
        />
        <TextLabel title={'Password'} />
        <TextInput
          // value={state.name}
          //   onChangeText={name => setState({...state, name})}
          style={styles.textInput}
          placeholder={'Password'}
          secureTextEntry
        />
        <TouchableOpacity
          activeOpacity={0.8}
          onPress={() => navigation.navigate('TabNavigator')}>
          <LinearGradient
            colors={['#C86DD7', '#3023AE']}
            start={{x: 0, y: 0}}
            end={{x: 1, y: 1}}
            style={styles.touchButton}>
            <Text style={styles.touchText}>SIGN IN</Text>
          </LinearGradient>
        </TouchableOpacity>
        <Text style={styles.bottomText}>
          Don’t have an account?{' '}
          <Text
            onPress={() => navigation.navigate('Ragister')}
            style={{color: '#3023AE'}}>
            Create now
          </Text>
        </Text>
      </KeyboardAwareScrollView>
    </View>
  );
};

export default Login;
const TextLabel = ({title}) => <Text style={styles.textLabel}>{title}</Text>;
const styles = StyleSheet.create({
  image: {
    marginTop: height / 8,
    width: 176,
    height: 336,
    resizeMode: 'contain',
  },
  text: {
    fontFamily: 'Nunito',
    fontSize: 35,
    fontWeight: '900',
    color: '#000000',
    marginTop: height / 4.3,
    marginLeft: 30,
  },
  textInput: {
    borderRadius: 10,
    borderWidth: 1,
    padding: 10,
    paddingHorizontal: 10,
    marginHorizontal: 30,
    marginTop: 10,
    marginBottom: 0,
    fontSize: 12,
    fontFamily: 'Nunito',
    fontFamily: '500',
    backgroundColor: '#F0F0F0',
    borderColor: '#F0F0F0',
  },
  textLabel: {
    fontFamily: 'Nunito',
    fontWeight: '900',
    fontSize: 14,
    color: '#0E1236',
    marginHorizontal: 30,
    marginTop: 30,
    marginBottom: 0,
  },
  touchButton: {
    padding: 10,
    marginHorizontal: 30,
    borderRadius: 23,
    marginTop: 50,
  },
  touchText: {
    fontFamily: 'Nunito',
    fontWeight: '900',
    fontSize: 16,
    color: '#fff',
    textAlign: 'center',
  },
  bottomText: {
    fontFamily: 'Nunito',
    fontWeight: '900',
    fontSize: 14,
    color: '#0C3637',
    marginTop: 10,
    textAlign: 'center',
    marginBottom: 20,
  },
});
