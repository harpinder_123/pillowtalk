import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
  ImageBackground,
  ProgressBarAndroidBase,
  TextInput,
  FlatList,
} from 'react-native';
import {StatusBarDark} from '../Custom/CustomStatusBar';
import {Header, MainView} from '../Custom/CustomView';
import * as Progress from 'react-native-progress';
import LinearGradient from 'react-native-linear-gradient';
const {height, width} = Dimensions.get('window');

const Ragister4 = ({navigation}) => {
  return (
    <View style={{backgroundColor: '#fff', flex: 1}}>
      <StatusBarDark />
      <Header onPress={() => navigation.goBack()} />
      <ScrollView>
        <ImageBackground
          style={styles.image}
          source={require('../images/background.png')}>
          <Progress.Bar
            progress={0.7}
            width={330}
            color={'#3023AE'}
            style={{
              borderColor: '#D8D8D8',
              backgroundColor: '#D8D8D8',
              alignSelf: 'center',
              marginTop: 20,
            }}
          />
          <Text style={styles.basicText}>About me</Text>
          <View style={styles.textInput}>
            <TextInput
              // value={state.name}
              //   onChangeText={name => setState({...state, name})}
              placeholder={'Write your description'}
            />
          </View>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              marginHorizontal: 20,
            }}>
            <Text style={styles.skip}>SKIP</Text>
            <TouchableOpacity
              activeOpacity={0.8}
              onPress={() => navigation.navigate('Ragister5')}
              style={styles.touch}>
              <Text style={styles.touchText}>NEXT</Text>
            </TouchableOpacity>
          </View>
        </ImageBackground>
      </ScrollView>
    </View>
  );
};

export default Ragister4;

const styles = StyleSheet.create({
  image: {
    width: '100%',
    height: '100%',
    resizeMode: 'contain',
    alignSelf: 'center',
  },
  basicText: {
    fontFamily: 'Nunito',
    fontSize: 34,
    fontWeight: '900',
    color: '#000000',
    marginTop: 60,
    textAlign: 'center',
  },
  containerImg: {
    width: 90,
    height: 90,
    alignSelf: 'center',
  },
  loremText: {
    fontFamily: 'Nunito',
    fontSize: 14,
    fontWeight: '900',
    color: '#FF0000',
    marginTop: 20,
    marginLeft: 30,
  },
  skip: {
    fontFamily: 'Nunito',
    fontSize: 20,
    fontWeight: '700',
    color: '#000000',
    marginTop: '95%',
  },
  touch: {
    width: '35%',
    height: 58,
    backgroundColor: '#3023AE',
    borderRadius: 30,
    marginTop: '90%',
    marginBottom: 20,
  },
  touchText: {
    fontFamily: 'Nunito',
    fontSize: 22,
    fontWeight: '700',
    color: '#fff',
    textAlign: 'center',
    marginTop: 12,
  },
  textInput: {
    borderRadius: 18,
    borderWidth: 1,
    padding: 10,
    marginHorizontal: 15,
    marginTop: 80,
    marginBottom: 0,
    fontSize: 12,
    fontFamily: 'Nunito',
    fontFamily: '500',
    backgroundColor: '#ffffff',
    borderColor: '#D8D8D8',
    height: 120,
  },
});
