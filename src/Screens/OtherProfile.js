import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
  ImageBackground,
  useWindowDimensions,
  FlatList,
} from 'react-native';
import {StatusBarDark} from '../Custom/CustomStatusBar';
const {height, width} = Dimensions.get('window');

const OtherProfile = ({navigation}) => {
  const [select, setSelect] = useState(0);
  const [data, setData] = useState([
    {id: '1', title: 'Music', backgroundColor: '#FCEAE6'},
    {id: '2', title: 'Photo', backgroundColor: '#E4F7FF'},
    {id: '3', title: 'Photography', backgroundColor: '#FFF1E4'},
    {id: '3', title: 'Design', backgroundColor: '#E5ECFF'},
    {id: '3', title: 'Art Film', backgroundColor: '#F7EFFC'},
    {id: '3', title: 'Dancing', backgroundColor: '#FFEFFC'},
  ]);
  return (
    <View style={{backgroundColor: '#FFFFFF', flex: 1}}>
      <StatusBarDark />
      <ScrollView>
        <ImageBackground
          style={styles.image}
          source={require('../images/images.png')}>
          <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <TouchableOpacity
              activeOpacity={0.8}
              onPress={() => navigation.navigate('TabNavigator')}>
              <Image
                style={{width: 30, height: 30, marginTop: 50, marginLeft: 25}}
                source={require('../images/arrowblack.png')}
              />
            </TouchableOpacity>
            <Image
              style={{width: 4, height: 20, marginTop: 50, marginRight: 20}}
              source={require('../images/dot.png')}
            />
          </View>
        </ImageBackground>
        <Text style={styles.text}>Martha Rojas</Text>
        <View style={{flexDirection: 'row'}}>
          <Text style={styles.subText}>500 Followers</Text>
          <TouchableOpacity
            activeOpacity={0.8}
            style={{marginLeft: 'auto'}}
            onPress={() => navigation.navigate('Followers')}>
            <Image
              style={{
                width: 40,
                height: 40,
                marginTop: -25,
              }}
              source={require('../images/like1.png')}
            />
          </TouchableOpacity>
          <TouchableOpacity
            activeOpacity={0.8}
            onPress={() => navigation.navigate('Rating')}>
            <Image
              style={{
                width: 40,
                height: 40,
                marginLeft: 10,
                marginHorizontal: 20,
                marginTop: -25,
              }}
              source={require('../images/star1.png')}
            />
          </TouchableOpacity>
        </View>
        <View style={{flexDirection: 'row', justifyContent: 'space-evenly'}}>
          <View>
            <Text style={styles.bottomtext}>Chat</Text>
            <View style={{flexDirection: 'row', marginLeft: 20}}>
              <Image
                style={{width: 20, height: 15, marginTop: 10}}
                source={require('../images/diamond.png')}
              />
              <Text style={styles.bottomsubtext}>0/mins</Text>
            </View>
          </View>
          <View>
            <Text style={styles.bottomtext}>Call</Text>
            <View style={{flexDirection: 'row', marginLeft: 20}}>
              <Image
                style={{width: 20, height: 15, marginTop: 10}}
                source={require('../images/diamond.png')}
              />
              <Text style={styles.bottomsubtext}>500/mins</Text>
            </View>
          </View>
          <View>
            <Text style={styles.bottomtext}>Video</Text>
            <View style={{flexDirection: 'row', marginLeft: 20}}>
              <Image
                style={{width: 20, height: 15, marginTop: 10}}
                source={require('../images/diamond.png')}
              />
              <Text style={styles.bottomsubtext}>400/mins</Text>
            </View>
          </View>
        </View>
        <Text style={styles.aboutText}>About Me</Text>
        <Text style={styles.aboutSubText}>
          Love music, cooking, swimming, going out,{'\n'}travelling etc. Wanna
          be friends ?
        </Text>
        <Text style={styles.private}>Private Album</Text>
        <View style={{flexDirection: 'row'}}>
          <ScrollView horizontal>
            <TouchableOpacity
              activeOpacity={0.8}
              onPress={() => navigation.navigate('PrivatePopup')}>
              <ImageBackground
                imageStyle={{borderRadius: 10}}
                style={{
                  width: 74,
                  height: 76,
                  marginTop: 20,
                  marginLeft: 25,
                }}
                source={require('../images/Group2.png')}>
                <Image
                  style={{
                    width: 16,
                    height: 20,
                    alignSelf: 'center',
                    marginTop: 25,
                  }}
                  source={require('../images/lock.png')}
                />
              </ImageBackground>
            </TouchableOpacity>
            <ImageBackground
              imageStyle={{borderRadius: 10}}
              style={{
                width: 74,
                height: 76,
                marginTop: 20,
                marginLeft: 15,
              }}
              source={require('../images/Group2.png')}>
              <Image
                style={{
                  width: 16,
                  height: 20,
                  alignSelf: 'center',
                  marginTop: 25,
                }}
                source={require('../images/lock.png')}
              />
            </ImageBackground>
            <ImageBackground
              imageStyle={{borderRadius: 10}}
              style={{
                width: 74,
                height: 76,
                marginTop: 20,
                marginLeft: 15,
              }}
              source={require('../images/Group2.png')}>
              <Image
                style={{
                  width: 16,
                  height: 20,
                  alignSelf: 'center',
                  marginTop: 25,
                }}
                source={require('../images/lock.png')}
              />
            </ImageBackground>
            <ImageBackground
              imageStyle={{borderRadius: 10}}
              style={{
                width: 74,
                height: 76,
                marginTop: 20,
                marginLeft: 15,
              }}
              source={require('../images/Group2.png')}>
              <Image
                style={{
                  width: 16,
                  height: 20,
                  alignSelf: 'center',
                  marginTop: 25,
                }}
                source={require('../images/lock.png')}
              />
            </ImageBackground>
            <ImageBackground
              imageStyle={{borderRadius: 10}}
              style={{
                width: 74,
                height: 76,
                marginTop: 20,
                marginLeft: 15,
              }}
              source={require('../images/Group2.png')}>
              <Image
                style={{
                  width: 16,
                  height: 20,
                  alignSelf: 'center',
                  marginTop: 25,
                }}
                source={require('../images/lock.png')}
              />
            </ImageBackground>
          </ScrollView>
        </View>

        <View style={{flexDirection: 'row'}}>
          <View>
            <Text style={styles.private}>Pictures</Text>
            <View style={styles.line} />
          </View>
          <Text style={styles.video}>Videos</Text>
        </View>
        <View style={{flexDirection: 'row'}}>
          <ScrollView horizontal>
            <Image
              style={{
                width: 83,
                height: 81,
                marginTop: 20,
                marginLeft: 25,
                borderRadius: 10,
              }}
              source={require('../images/Rectangle1.png')}
            />
            <Image
              style={{
                width: 83,
                height: 81,
                marginTop: 20,
                marginLeft: 15,
                borderRadius: 10,
              }}
              source={require('../images/Rectangle1.png')}
            />
            <Image
              style={{
                width: 83,
                height: 81,
                marginTop: 20,
                marginLeft: 15,
                borderRadius: 10,
              }}
              source={require('../images/Rectangle1.png')}
            />
            <Image
              style={{
                width: 83,
                height: 81,
                marginTop: 20,
                marginLeft: 15,
                borderRadius: 10,
              }}
              source={require('../images/Rectangle1.png')}
            />
          </ScrollView>
        </View>
        <Text style={styles.interest}>Interest</Text>
        <View style={{marginLeft: 25, marginTop: 10}}>
          <FlatList
            numColumns={3}
            keyExtractor={item => item.id}
            data={data}
            renderItem={({item, index}) => (
              <TouchableOpacity
                onPress={() => setSelect(index)}
                style={[styles.touch, {backgroundColor: item.backgroundColor}]}>
                <Text
                  style={{
                    fontFamily: 'Nunito',
                    fontSize: 14,
                    fontWeight: '700',
                    color: '#363636',
                    // marginTop: 10,
                    textAlign: 'center',
                  }}>
                  {item.title}
                </Text>
              </TouchableOpacity>
            )}
          />
        </View>
        <View
          style={{
            flexDirection: 'row',
            alignSelf: 'center',
            marginTop: 20,
            marginBottom: 20,
          }}>
          <Image
            style={{width: 60, height: 60, margin: 5}}
            source={require('../images/call-circle.png')}
          />
          <Image
            style={{width: 60, height: 60, margin: 5}}
            source={require('../images/circle-chat.png')}
          />
          <Image
            style={{width: 60, height: 60, margin: 5}}
            source={require('../images/circle-video.png')}
          />
        </View>
      </ScrollView>
    </View>
  );
};

export default OtherProfile;

const styles = StyleSheet.create({
  image: {
    width: '100%',
    height: 350,
  },
  text: {
    fontSize: 28,
    fontFamily: 'Nunito',
    fontWeight: '600',
    color: '#363636',
    marginLeft: 25,
    marginTop: 20,
  },
  subText: {
    fontSize: 14,
    fontFamily: 'Nunito',
    fontWeight: '600',
    color: '#3023AE',
    marginTop: 5,
    marginLeft: 25,
  },
  bottomtext: {
    fontFamily: 'Nunito',
    fontSize: 12,
    fontWeight: '500',
    color: '#ACB1C0',
    marginTop: 20,
    marginLeft: 20,
    // textAlign: 'center',
  },
  bottomsubtext: {
    fontFamily: 'Nunito',
    fontSize: 14,
    fontWeight: 'bold',
    color: '#000000',
    marginTop: 7,
    marginLeft: 5,
    // textAlign: 'center',
  },
  aboutText: {
    fontFamily: 'Nunito',
    fontSize: 20,
    fontWeight: '700',
    color: '#363636',
    marginLeft: 25,
    marginTop: 20,
  },
  aboutSubText: {
    fontFamily: 'Nunito',
    fontSize: 16,
    fontWeight: '600',
    color: '#ACB1C0',
    marginLeft: 25,
    marginTop: 5,
  },
  private: {
    fontFamily: 'Nunito',
    fontSize: 16,
    fontWeight: '700',
    color: '#3023AE',
    marginLeft: 25,
    marginTop: 20,
  },
  video: {
    fontFamily: 'Nunito',
    fontSize: 16,
    fontWeight: '700',
    color: '#ACB1C0',
    marginLeft: 25,
    marginTop: 20,
  },
  line: {
    backgroundColor: '#3023AE',
    height: 3,
    width: '70%',
    marginHorizontal: 25,
    marginTop: 5,
  },
  interest: {
    fontFamily: 'Nunito',
    fontSize: 20,
    fontWeight: '700',
    color: '#363636',
    marginLeft: 25,
    marginTop: 20,
  },
  touch: {
    width: width / 3 - 25,
    paddingHorizontal: 10,
    paddingVertical: 8,
    borderRadius: 16,
    marginTop: 5,
    margin: 5,
  },
});
